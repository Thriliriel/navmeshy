﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GenerateAgents : MonoBehaviour {
    //qnt agents
    public int qntAgents;
    //beautiful agent prefab array
    public GameObject[] agent;
    //walking filename
    public string walkFilename;
    //fps
    public int fps;
    //walking file
    private StreamReader theReader;

    public void CreateAgents()
    {
        GameObject[] agents = GameObject.FindGameObjectsWithTag("Agent");
        foreach(GameObject ag in agents)
        {
            GameObject.DestroyImmediate(ag);
        }

        //private List<Vector3> allPositionsByFrame;
        for(int i = 0; i < qntAgents; i++)
        {
            GameObject newAgent = Instantiate(agent[Random.Range(0, agent.Length)], Vector3.zero, Quaternion.identity) as GameObject;
            newAgent.name = "agent" + (i);
            newAgent.GetComponent<WalkController>().fps = fps;
            newAgent.GetComponent<WalkController>().StartList();

            //scale
            newAgent.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
        }
    }

    public void ReadFile()
    {
        theReader = new StreamReader(Application.dataPath + "/" + walkFilename, System.Text.Encoding.Default);
        string line;

        using (theReader)
        {
            int lineCount = 1;
            // While there's lines left in the text file, do this:
            do
            {
                line = theReader.ReadLine();

                if (line != null)
                {
                    //each line 1 agent, separated by ";"
                    string[] entries = line.Split(';');

                    //get agent name
                    string agentName = entries[1];

                    GameObject thisAgent = GameObject.Find(agentName);
                    if (thisAgent)
                    {
                        thisAgent.GetComponent<WalkController>().allPositionsByFrame.Add(new Vector3(System.Convert.ToSingle(entries[2]), System.Convert.ToSingle(entries[3]), System.Convert.ToSingle(entries[4])));
                    }
                }
                lineCount++;
            }
            while (line != null);
        }

        //close it
        theReader.Close();
    }
}
