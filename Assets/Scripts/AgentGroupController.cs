﻿using System.Collections.Generic;
using UnityEngine;

public class AgentGroupController : MonoBehaviour {

    //Hofstede class
    public HofstedeClass hofstede;
    //agents of this groups
    public List<GameObject> agents;
    //cell
    public GameObject cell;
    //goals schedule
    public List<GameObject> go;
    //goals intentions
    public List<float> intentions;
    //goals desire
    public List<float> desire;
    //max speed
    public float maxSpeed;

    // Use this for initialization
    void Awake () {
        agents = new List<GameObject>();
        hofstede = new HofstedeClass();
	}
	
	// Update is called once per frame
	/*void Update () {
		
	}*/

    //reorder goals/intentions
    public void ReorderGoals()
    {
        for (int i = 0; i < intentions.Count; i++)
        {
            for (int j = i + 1; j < intentions.Count; j++)
            {
                //if j element is bigger, change
                if (intentions[i] < intentions[j])
                {
                    //reorder intentions
                    float temp = intentions[j];
                    intentions[j] = intentions[i];
                    intentions[i] = temp;

                    //reorder desires
                    float tempD = desire[j];
                    desire[j] = desire[i];
                    desire[i] = tempD;

                    //reorder goals
                    GameObject tempG = go[j];
                    go[j] = go[i];
                    go[i] = tempG;
                }
            }
        }
    }
}
