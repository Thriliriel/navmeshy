﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    //scenario X
    public float scenarioSizeX;
    //scenario Z
    public float scenarioSizeZ;
    //agent prefab
    public GameObject agent;
    //agent max speed
    public float agentMaxSpeed;
    //agent radius
    public float agentRadius;
    //cell radius
    public float cellRadius;
    //cell prefab
    public GameObject cell;
    //sign prefab
    public GameObject sign;
    //goal prefab
    public GameObject goalP;
    //agent group prefab
    public GameObject agentGroup;
    //qnt of agents in the scene
    public int qntAgents;
    //qnt of signs in the scene
    public int qntSigns;
    //qnt of goals in the scene
    public int qntGoals;
    //radius for auxin collide
    public float auxinRadius;
    //save config file?
    public bool saveConfigFile;
    //load config file?
    public bool loadConfigFile;
    //all simulation files directory
    public string allSimulations;
    //config filename
    public string configFilename;
    //obstacles filename
    public string obstaclesFilename;
    //schedule filename
    public string scheduleFilename;
    //exit filename
    public string exitFilename;
    //signs filename
    public string signsFilename;
    //goals filename
    public string goalsFilename;
    //exit agents/goal filename
    public string agentsGoalFilename;
    //exit interactions filename
    public string interactionsFilename;
    //canvas text
    //public Transform canvasText;
    //files controller
    public FilesController filesController;
    //last frame count
    public int lastFrameCount = 0;
    //obstacle displacement (since the obstacle is not at Unity 0,0,0)
    public float obstacleDisplacement;
    //qnt groups (0 value means no groups, where each agent will be alone inside a group)
    public int qntGroups;
    //use Hofstede?
    public bool useHofstede;

    //auxins density
    //private float PORC_QTD_Marcacoes = 0.65f;
    private float PORC_QTD_Marcacoes = 0.8f;
    //private float PORC_QTD_Marcacoes = 1f;
    //qnt of auxins on the ground
    private int qntAuxins;
    //all config directories
    private string[] allDirs;
    //simulation index
    private int simulationIndex = 0;
    //stop all sims
    private bool gameOver = false;
    //store all cells
    private GameObject[] allCells;
    //terrain
    private Terrain terrain;
    //intention threshold
    private float intentionThreshold = 0.8f;
    //signs instantiated positions
    private List<Vector3> positionsSigns;
    //all obstacles
    private GameObject[] allObstacles;
    //keep the agents already destroyed
    private List<int> agentsDestroyed = new List<int>();
    //text element
    private Text text;
    //time elapsed (to show on the screen)
    private float elapsed;
    //the time it shows in the screen
    private int showTime;
    //last frames count to calculate the FPS
    private int lastframesCounter;

    //on destroy application, close the exit files
    void OnDestroy()
    {
        filesController.Finish();
    }

    // Use this for initialization
    void Awake()
    {
        if (qntGroups == 0 || qntGroups > qntAgents)
        {
            qntGroups = qntAgents;
        }

        //get the text element
        //text = canvasText.GetComponent<Text>();

        elapsed = showTime = lastframesCounter = 0;

        //start the positionSigns list
        positionsSigns = new List<Vector3>();

        //get all subdirectories within the defined config directory
        allDirs = Directory.GetDirectories(Application.dataPath + "/" + allSimulations);

        //reorder to make the numbers right (0 1 2 3, instead 0 1 10 11)
        System.Array.Sort(allDirs, delegate (string a, string b)
        {
            string[] breakA = a.Split('_');
            string[] breakB = b.Split('_');
            int indA = System.Int32.Parse(breakA[breakA.Length - 1]);
            int indB = System.Int32.Parse(breakB[breakB.Length - 1]);
            return (indA).CompareTo(indB);
        });
        /*foreach (string dir in allDirs) {
            Debug.Log(dir);
        }*/

        //camera height and center position
        ConfigCamera();

        //find the Terrain gameObject and his terrain component
        terrain = GameObject.Find("Terrain").GetComponent<Terrain>();
        //change terrain size according informed
        terrain.terrainData.size = new Vector3(scenarioSizeX, terrain.terrainData.size.y, scenarioSizeZ);

        //get all obstacles (which should already be on the scene, along cells, goals, signs and markers)
        allObstacles = GameObject.FindGameObjectsWithTag("Obstacle");

        //if loadConfigFile is checked, we do not generate random agents and signs. We load it from the agents.dat and signs.dat
        if (loadConfigFile)
        {
            LoadChainSimulation();
        }//else, generate anew
        else
        {
            GenerateAnew();
        }

        //set the text
        //text.text = "Simulation " + simulationIndex + " Started!";

        //get all cells in scene
        allCells = GameObject.FindGameObjectsWithTag("Cell");
    }

    void Start()
    {
        //all ready to go. If saveConfigFile is checked, save this config in a csv file
        if (saveConfigFile)
        {
            filesController.SaveConfigFile(cellRadius, auxinRadius, allObstacles);
        }

        //change timeScale (make things faster)
        //seems to not be so good, since it calculates just once for a great amount of time
        //Time.timeScale = 5f;
    }

    // Update is called once per frame
    void Update()
    {
        //if simulation should be running yet
        if (!gameOver)
        {
            // Update the text
            elapsed += Time.deltaTime;
            if(elapsed > 1f)
            {
                elapsed -= 1f;
                showTime++;

                //text.text = "Simulation " + simulationIndex + " Running\nTime: " + showTime + "\nEstimated FPS: " + ((Time.frameCount - lastFrameCount) - lastframesCounter);

                lastframesCounter = (Time.frameCount - lastFrameCount);
            }

            //reset auxins
            //for each agent, we reset their auxins
            for (int i = 0; i < qntAgents; i++)
            {
                //first, lets see if the agent is still in the scene
                bool destroyed = false;
                for (int j = 0; j < agentsDestroyed.Count; j++)
                {
                    if (agentsDestroyed[j] == i) destroyed = true;
                }

                //if he is
                if (!destroyed)
                {
                    GameObject agentI = GameObject.Find("agent" + i);
                    List<AuxinController> axAge = agentI.GetComponent<AgentController>().GetAuxins();
                    for (int j = 0; j < axAge.Count; j++)
                    {
                        axAge[j].ResetAuxin();
                    }
                    // Debug.Log(axAge.Count);
                }
            }

            //find nearest auxins for each agent
            for (int i = 0; i < qntAgents; i++)
            {
                //first, lets see if the agent is still in the scene
                bool destroyed = false;
                for (int j = 0; j < agentsDestroyed.Count; j++)
                {
                    if (agentsDestroyed[j] == i) destroyed = true;
                }

                //if he is
                if (!destroyed)
                {
                    GameObject agentI = GameObject.Find("agent" + i);
                    //find all auxins near him (Voronoi Diagram)
                    agentI.GetComponent<AgentController>().FindNearAuxins(cellRadius);
                }
            }

            /*to find where the agent must move, we need to get the vectors from the agent to each auxin he has, and compare with 
            the vector from agent to goal, generating a angle which must lie between 0 (best case) and 180 (worst case)
            The calculation formula was taken from the Bicho´s mastery tesis and from Paravisi algorithm, all included
            in AgentController.
            */

            /*for each agent, we:
            1 - verify if he is in the scene. If he is...
            2 - find him 
            3 - for each auxin near him, find the distance vector between it and the agent
            4 - calculate the movement vector (CalculateMotionVector())
            5 - calculate speed vector (CalculateSpeed())
            6 - walk (Walk())
            7 - verify if the agent has reached the goal. If so, destroy it
            */
            for (int i = 0; i < qntAgents; i++)
            {
                //verify if agent is not destroyed
                bool destroyed = false;
                for (int j = 0; j < agentsDestroyed.Count; j++)
                {
                    if (agentsDestroyed[j] == i) destroyed = true;
                }

                if (!destroyed)
                {
                    //find the agent
                    GameObject agentI = GameObject.Find("agent" + i);
                    AgentController agentIController = agentI.GetComponent<AgentController>();
                    GameObject goal = agentIController.go[0];
                    List<AuxinController> agentAuxins = agentIController.GetAuxins();

                    //vector for each auxin
                    for (int j = 0; j < agentAuxins.Count; j++)
                    {
                        //add the distance vector between it and the agent
                        agentIController.vetorDistRelacaoMarcacao.Add(agentAuxins[j].position - agentI.transform.position);

                        //just draw the little spider legs xD
                        Debug.DrawLine(agentAuxins[j].position, agentI.transform.position);
                    }

                    //calculate the movement vector
                    agentIController.CalculateMotionVector();
                    //calculate speed vector
                    agentIController.CalculateSpeed();
                    
                    //now, we check if agent is stuck with another agent
                    //if so, change places
                    if (agentIController.speed.Equals(Vector3.zero))
                    {
                        Collider[] lockHit = Physics.OverlapSphere(agentI.transform.position, agentRadius*2);
                        foreach (Collider loki in lockHit)
                        {
                            //if it is the Player tag (agent) and it is not the agent itself and he can change position (to avoid forever changing)
                            if (loki.gameObject.tag == "Player" && loki.gameObject.name != agentI.gameObject.name && agentIController.changePosition && loki.GetComponent<AgentController>().speed.Equals(Vector3.zero))
                            {
                                //the other agent will not change position in this frame
                                loki.GetComponent<AgentController>().changePosition = false;
                                //Debug.Log(agentI.gameObject.name + " -- " + loki.gameObject.name);
                                //exchange!!!
                                Vector3 positionA = agentI.transform.position;
                                agentI.transform.position = loki.gameObject.transform.position;
                                loki.gameObject.transform.position = positionA;
                            }
                        }
                    }

                    //walk
                    agentIController.Walk();

                    //verify agent position, in relation to the goal.
                    //if the distance between them is less than 1 (arbitrary, maybe authors have a better solution), he arrived. Destroy it so
                    float dist = Vector3.Distance(goal.transform.position, agentI.transform.position);
                    if (dist < agentIController.agentRadius * 5)
                    {
                        //if we are already at the last agent goal, he arrived
                        //if he has 2 goals yet, but the second one is the Looking For, he arrived too
                        if (agentIController.go.Count == 1 ||
                            (agentIController.go.Count == 2 && agentIController.go[1].gameObject.tag == "LookingFor"))
                        {
                            //save on the file
                            filesController.SaveAgentsGoalFile(agentI.name, goal.name, lastFrameCount);
                            //destroy it
                            agentsDestroyed.Add(i);
                            agentIController.group.GetComponent<AgentGroupController>().agents.Remove(agentI);
                            Destroy(agentI);
                        }//else, he must go to the next goal. Remove this actual goal and this intention
                        else
                        {
                            //before we remove his actual go, we check if it is the looking for state.
                            //if it is, we remove it, but add a new one, because he dont know where to go yet
                            bool newLookingFor = false;
                            if (agentIController.go[0].gameObject.tag == "LookingFor")
                            {
                                Destroy(agentIController.go[0].gameObject);
                                newLookingFor = true;
                            }//else, it is a goal. Save on the file
                            else
                            {
                                //save on the file
                                filesController.SaveAgentsGoalFile(agentI.name, goal.name, lastFrameCount);
                            }
                            agentIController.go.RemoveAt(0);
                            agentIController.intentions.RemoveAt(0);
                            agentIController.RemoveDesire(0);

                            if (newLookingFor)
                            {
                                //add the Looking For state, with a random position
                                GameObject lookingFor = GenerateLookingFor();
                                agentIController.go.Add(lookingFor);
                                agentIController.intentions.Add(intentionThreshold);
                                agentIController.AddDesire(1);

                                //since we have a new one, reorder
                                agentIController.ReorderGoals();
                            }
                        }
                    }
                }
            }

            //after all agents walked, find the centroid of all agents to update center position of each group
            GameObject[] agentsGroups = GameObject.FindGameObjectsWithTag("AgentGroup");
            foreach (GameObject ag in agentsGroups)
            {
                //just if there are agents in the group yet
                if (ag.GetComponent<AgentGroupController>().agents.Count > 0)
                {
                    Vector3 center = Vector3.zero;
                    for (int j = 0; j < ag.GetComponent<AgentGroupController>().agents.Count; j++)
                    {
                        center = center + ag.GetComponent<AgentGroupController>().agents[j].transform.position;
                    }
                    Vector3 newPosition = center / (float)ag.GetComponent<AgentGroupController>().agents.Count;
                    ag.transform.position = newPosition;
                }
            }

            //write the exit file
            filesController.SaveExitFile(lastFrameCount);
        }

        //End simulation?
        if (loadConfigFile)
        {
            EndSimulation();
        }
    }

    //draw cells
    public void DrawCells()
    {
        //if it is not set yet
        if (!terrain)
        {
            terrain = GameObject.Find("Terrain").GetComponent<Terrain>();
        }

        //reset the terrain size with the defined size
        terrain.terrainData.size = new Vector3(scenarioSizeX, terrain.terrainData.size.y, scenarioSizeZ);

        //get the cells parent
        GameObject cells = GameObject.Find("Cells");

        //first of all, create all cells (with this scene and this agentRadius)
        //since radius = 1; diameter = 2. So, iterate cellRadius*2
        //if the radius varies, this 2 operations adjust the cells
        Vector3 newPosition = new Vector3(cell.transform.position.x * cellRadius,
            cell.transform.position.y * cellRadius, cell.transform.position.z * cellRadius);
        Vector3 newScale = new Vector3(cell.transform.localScale.x * cellRadius,
            cell.transform.localScale.y * cellRadius, cell.transform.localScale.z * cellRadius);

        for (float i = 0; i < terrain.terrainData.size.x; i = i + cellRadius * 2)
        {
            for (float j = 0; j < terrain.terrainData.size.z; j = j + cellRadius * 2)
            {
                //verify if collides with some obstacle. We dont need cells in them.
                //for that, we need to check all 4 vertices of the cell. Otherwise, we may not have cells in some free spaces (for example, half of a cell would be covered by an obstacle, so that cell
                //would not be instantied)
                bool collideRight = CheckObstacle(new Vector3(newPosition.x + i + cellRadius, newPosition.y, newPosition.z + j), "Obstacle", 0.01f);
                bool collideLeft = CheckObstacle(new Vector3(newPosition.x + i - cellRadius, newPosition.y, newPosition.z + j), "Obstacle", 0.01f);
                bool collideTop = CheckObstacle(new Vector3(newPosition.x + i, newPosition.y, newPosition.z + j + cellRadius), "Obstacle", 0.01f);
                bool collideDown = CheckObstacle(new Vector3(newPosition.x + i, newPosition.y, newPosition.z + j - cellRadius), "Obstacle", 0.01f);
                bool collideRightTop = CheckObstacle(new Vector3(newPosition.x + i + cellRadius, newPosition.y, newPosition.z + j + cellRadius), "Obstacle", 0.01f);
                bool collideLeftBottom = CheckObstacle(new Vector3(newPosition.x + i - cellRadius, newPosition.y, newPosition.z + j - cellRadius), "Obstacle", 0.01f);
                bool collideTopLeft = CheckObstacle(new Vector3(newPosition.x + i - cellRadius, newPosition.y, newPosition.z + j + cellRadius), "Obstacle", 0.01f);
                bool collideDownRight = CheckObstacle(new Vector3(newPosition.x + i + cellRadius, newPosition.y, newPosition.z + j - cellRadius), "Obstacle", 0.01f);
                
                //if did collide it all, means we have found at least 1 obstacle in each case. So, the cell is covered by an obstacle
                //otherwise, we go on
                if (!collideRight || !collideLeft || !collideTop || !collideDown || !collideRightTop || !collideLeftBottom || !collideTopLeft || !collideDownRight)
                {
                    //instantiante a new cell
                    GameObject newCell = Instantiate(cell, new Vector3(newPosition.x + i, newPosition.y, newPosition.z + j), Quaternion.identity) as GameObject;
                    //change his name
                    newCell.name = "cell" + i + "-" + j;
                    //change scale
                    newCell.transform.localScale = newScale;
                    //change parent
                    newCell.transform.parent = cells.transform;
                    //start list
                    newCell.GetComponent<CellController>().StartList();
                }
            }
        }

        //get all cells in scene
        allCells = GameObject.FindGameObjectsWithTag("Cell");
        //just to see how many cells were generated
        Debug.Log(allCells.Length);
    }

    //place auxins
    public void PlaceAuxins()
    {
        //get all cells in scene
        allCells = GameObject.FindGameObjectsWithTag("Cell");

        //lets set the qntAuxins for each cell according the density estimation
        float densityToQnt = PORC_QTD_Marcacoes;

        densityToQnt *= (cellRadius * 2f) / (2.0f * auxinRadius);
        densityToQnt *= (cellRadius * 2f) / (2.0f * auxinRadius);

        qntAuxins = (int)Mathf.Floor(densityToQnt);
        Debug.Log(qntAuxins);

        //for each cell, we generate his auxins
        for (int c = 0; c < allCells.Length; c++)
        {
            //Dart throwing auxins
            DartThrowMarkers(c);
            //Debug.Log(allCells[c].name+"--"+allCells[c].GetComponent<CellController>().GetAuxins().Count);
        }
    }

    //draw obstacles on the scene
    public void DrawObstacles()
    {
        //erase all obstacles
        GameObject[] obstaclesOnScene = GameObject.FindGameObjectsWithTag("Obstacle");
        foreach(GameObject obs in obstaclesOnScene)
        {
            DestroyImmediate(obs);
        }

        //draw rectangle 1
        /*Vector3[] vertices = new Vector3[4];
        vertices[0] = new Vector3(10, 0, 10);
        vertices[1] = new Vector3(10, 0, 40);
        vertices[2] = new Vector3(40, 0, 40);
        vertices[3] = new Vector3(40, 0, 10);

        int[] triangles = new int[6];
        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 2;
        triangles[4] = 3;
        triangles[5] = 0;

        DrawObstacle(vertices, triangles);

        //draw rectangle 2
        vertices = new Vector3[4];
        vertices[0] = new Vector3(60, 0, 10);
        vertices[1] = new Vector3(60, 0, 40);
        vertices[2] = new Vector3(90, 0, 40);
        vertices[3] = new Vector3(90, 0, 10);

        triangles = new int[6];
        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 2;
        triangles[4] = 3;
        triangles[5] = 0;

        DrawObstacle(vertices, triangles);

        //draw rectangle 3
        vertices = new Vector3[4];
        vertices[0] = new Vector3(10, 0, 60);
        vertices[1] = new Vector3(10, 0, 90);
        vertices[2] = new Vector3(40, 0, 90);
        vertices[3] = new Vector3(40, 0, 60);

        triangles = new int[6];
        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 2;
        triangles[4] = 3;
        triangles[5] = 0;

        DrawObstacle(vertices, triangles);

        //draw rectangle 4
        vertices = new Vector3[4];
        vertices[0] = new Vector3(60, 0, 60);
        vertices[1] = new Vector3(60, 0, 90);
        vertices[2] = new Vector3(90, 0, 90);
        vertices[3] = new Vector3(90, 0, 60);

        triangles = new int[6];
        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 2;
        triangles[4] = 3;
        triangles[5] = 0;

        DrawObstacle(vertices, triangles);*/

        //drawing eiffel scenario
        //draw big garden - left
        /*Vector3[] vertices = new Vector3[4];
        vertices[0] = new Vector3(0, 0, 0);
        vertices[1] = new Vector3(0, 0, 41);
        vertices[2] = new Vector3(33, 0, 41);
        vertices[3] = new Vector3(33, 0, 0);

        int[] triangles = new int[6];
        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 2;
        triangles[4] = 3;
        triangles[5] = 0;

        DrawObstacle(vertices, triangles);

        //draw big garden - right
        vertices = new Vector3[4];
        vertices[0] = new Vector3(41.5f, 0, 0);
        vertices[1] = new Vector3(41.5f, 0, 41);
        vertices[2] = new Vector3(75, 0, 41);
        vertices[3] = new Vector3(75, 0, 0);
        
        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 2;
        triangles[4] = 3;
        triangles[5] = 0;

        DrawObstacle(vertices, triangles);

        //middle little garden - left
        vertices = new Vector3[4];
        vertices[0] = new Vector3(0, 0, 43.5f);
        vertices[1] = new Vector3(0, 0, 44.5f);
        vertices[2] = new Vector3(32.1f, 0, 44.5f);
        vertices[3] = new Vector3(32.1f, 0, 43.5f);

        triangles = new int[6];
        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 2;
        triangles[4] = 3;
        triangles[5] = 0;

        DrawObstacle(vertices, triangles);

        //middle little garden - right
        vertices = new Vector3[4];
        vertices[0] = new Vector3(42, 0, 43.5f);
        vertices[1] = new Vector3(42, 0, 44.5f);
        vertices[2] = new Vector3(75, 0, 44.5f);
        vertices[3] = new Vector3(75, 0, 43.5f);

        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 2;
        triangles[4] = 3;
        triangles[5] = 0;

        DrawObstacle(vertices, triangles);

        //Siena river - left
        vertices = new Vector3[4];
        vertices[0] = new Vector3(0, 0, 47);
        vertices[1] = new Vector3(0, 0, 74.8f);
        vertices[2] = new Vector3(33.5f, 0, 74.8f);
        vertices[3] = new Vector3(33.5f, 0, 47);

        triangles = new int[6];
        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 2;
        triangles[4] = 3;
        triangles[5] = 0;

        DrawObstacle(vertices, triangles);

        //Siena river - right
        vertices = new Vector3[4];
        vertices[0] = new Vector3(41, 0, 47);
        vertices[1] = new Vector3(41, 0, 74.8f);
        vertices[2] = new Vector3(75, 0, 74.8f);
        vertices[3] = new Vector3(75, 0, 47);

        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 2;
        triangles[4] = 3;
        triangles[5] = 0;

        DrawObstacle(vertices, triangles);

        //little upper garden - left
        vertices = new Vector3[4];
        vertices[0] = new Vector3(0, 0, 77.1f);
        vertices[1] = new Vector3(0, 0, 82.7f);
        vertices[2] = new Vector3(32.9f, 0, 82.7f);
        vertices[3] = new Vector3(32.9f, 0, 77.1f);

        triangles = new int[6];
        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 2;
        triangles[4] = 3;
        triangles[5] = 0;

        DrawObstacle(vertices, triangles);

        //little upper garden - right
        vertices = new Vector3[4];
        vertices[0] = new Vector3(41.5f, 0, 77.1f);
        vertices[1] = new Vector3(41.5f, 0, 82.7f);
        vertices[2] = new Vector3(75, 0, 82.7f);
        vertices[3] = new Vector3(75, 0, 77.1f);

        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 2;
        triangles[4] = 3;
        triangles[5] = 0;

        DrawObstacle(vertices, triangles);*/

        //draw Venice
        //water column 1
        /*Vector3[] vertices = new Vector3[4];
        vertices[0] = new Vector3(0, 0, 74);
        vertices[1] = new Vector3(0, 0, 116);
        vertices[2] = new Vector3(10.5f, 0, 116);
        vertices[3] = new Vector3(10.5f, 0, 74);

        int[] triangles = new int[6];
        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 2;
        triangles[4] = 3;
        triangles[5] = 0;

        DrawObstacle(vertices, triangles);

        //water column 1-2
        vertices = new Vector3[4];
        vertices[0] = new Vector3(10.5f, 0, 74);
        vertices[1] = new Vector3(10.5f, 0, 116);
        vertices[2] = new Vector3(12, 0, 116);
        vertices[3] = new Vector3(12, 0, 74);

        DrawObstacle(vertices, triangles);

        //water column 2
        vertices = new Vector3[4];
        vertices[0] = new Vector3(12f, 0, 74);
        vertices[1] = new Vector3(12f, 0, 116);
        vertices[2] = new Vector3(42.5f, 0, 116);
        vertices[3] = new Vector3(42.5f, 0, 87.5f);

        DrawObstacle(vertices, triangles);

        //water column 2-3
        vertices = new Vector3[4];
        vertices[0] = new Vector3(42.5f, 0, 87.5f);
        vertices[1] = new Vector3(42.5f, 0, 116);
        vertices[2] = new Vector3(45, 0, 116);
        vertices[3] = new Vector3(45, 0, 88.5f);

        DrawObstacle(vertices, triangles);

        //water column 3
        vertices = new Vector3[4];
        vertices[0] = new Vector3(45, 0, 88.5f);
        vertices[1] = new Vector3(45, 0, 116);
        vertices[2] = new Vector3(80, 0, 116);
        vertices[3] = new Vector3(80, 0, 99f);

        DrawObstacle(vertices, triangles);

        //water column 3-4
        vertices = new Vector3[4];
        vertices[0] = new Vector3(80, 0, 99);
        vertices[1] = new Vector3(80, 0, 116);
        vertices[2] = new Vector3(82, 0, 116);
        vertices[3] = new Vector3(82, 0, 100);

        DrawObstacle(vertices, triangles);

        //water column 4
        vertices = new Vector3[4];
        vertices[0] = new Vector3(82, 0, 100);
        vertices[1] = new Vector3(82, 0, 116);
        vertices[2] = new Vector3(112.5f, 0, 116);
        vertices[3] = new Vector3(112.5f, 0, 100);

        DrawObstacle(vertices, triangles);

        //water arrangement after column 4
        vertices = new Vector3[4];
        vertices[0] = new Vector3(112.5f, 0, 100);
        vertices[1] = new Vector3(112.5f, 0, 116);
        vertices[2] = new Vector3(118, 0, 116);
        vertices[3] = new Vector3(113.5f, 0, 107.5f);

        triangles = new int[6];
        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 3;
        triangles[3] = 1;
        triangles[4] = 2;
        triangles[5] = 3;

        DrawObstacle(vertices, triangles);

        //water bend right
        vertices = new Vector3[4];
        vertices[0] = new Vector3(112.5f, 0, 100);
        vertices[1] = new Vector3(113.5f, 0, 107.5f);
        vertices[2] = new Vector3(138, 0, 91);
        vertices[3] = new Vector3(131.5f, 0, 87.5f);

        triangles = new int[6];
        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 2;
        triangles[4] = 3;
        triangles[5] = 0;

        DrawObstacle(vertices, triangles);

        //water bend right 2
        vertices = new Vector3[4];
        vertices[0] = new Vector3(131.5f, 0, 87.5f);
        vertices[1] = new Vector3(138, 0, 91);
        vertices[2] = new Vector3(143, 0, 66.5f);
        vertices[3] = new Vector3(137f, 0, 66.5f);

        DrawObstacle(vertices, triangles);

        //water bend right 3
        vertices = new Vector3[4];
        vertices[0] = new Vector3(137f, 0, 66.5f);
        vertices[1] = new Vector3(143, 0, 66.5f);
        vertices[2] = new Vector3(144.5f, 0, 59);
        vertices[3] = new Vector3(133, 0, 57);

        DrawObstacle(vertices, triangles);

        //water bend right 4
        vertices = new Vector3[4];
        vertices[0] = new Vector3(133, 0, 57);
        vertices[1] = new Vector3(137.5f, 0, 52.5f);
        vertices[2] = new Vector3(131.5f, 0, 48);
        vertices[3] = new Vector3(129, 0, 55.7f);

        DrawObstacle(vertices, triangles);

        //water bend flow to left
        vertices = new Vector3[4];
        vertices[0] = new Vector3(127, 0, 54.5f);
        vertices[1] = new Vector3(129.5f, 0, 47);
        vertices[2] = new Vector3(91f, 0, 30);
        vertices[3] = new Vector3(86.5f, 0, 36.5f);

        DrawObstacle(vertices, triangles);

        //water follow to left
        vertices = new Vector3[4];
        vertices[0] = new Vector3(74.5f, 0, 22f);
        vertices[1] = new Vector3(78, 0, 32.5f);
        vertices[2] = new Vector3(84.5f, 0, 36f);
        vertices[3] = new Vector3(89f, 0, 29);

        DrawObstacle(vertices, triangles);

        //water follow to left 2
        vertices = new Vector3[4];
        vertices[0] = new Vector3(78, 0, 32.5f); 
        vertices[1] = new Vector3(74.5f, 0, 22f);
        vertices[2] = new Vector3(71.5f, 0, 18.5f);
        vertices[3] = new Vector3(67, 0, 23f);

        DrawObstacle(vertices, triangles);

        //water follow to left 3
        vertices = new Vector3[4];
        vertices[0] = new Vector3(67, 0, 23f);
        vertices[1] = new Vector3(71.5f, 0, 18.5f);
        vertices[2] = new Vector3(59f, 0, 5.8f);
        vertices[3] = new Vector3(53.8f, 0, 10.5f);

        DrawObstacle(vertices, triangles);

        //water follow to left 4
        vertices = new Vector3[4];
        vertices[0] = new Vector3(53.8f, 0, 10.5f);
        vertices[1] = new Vector3(59f, 0, 5.8f);
        vertices[2] = new Vector3(59.5f, 0, 4.5f);
        vertices[3] = new Vector3(53.8f, 0, 4.5f);

        DrawObstacle(vertices, triangles);

        //water turn to bottom
        vertices = new Vector3[4];
        vertices[0] = new Vector3(53.8f, 0, 4.5f);
        vertices[1] = new Vector3(59.5f, 0, 4.5f);
        vertices[2] = new Vector3(60.5f, 0, 2);
        vertices[3] = new Vector3(54.5f, 0, 2);

        DrawObstacle(vertices, triangles);

        //water follow to left bottom
        vertices = new Vector3[4];
        vertices[0] = new Vector3(55, 0, 0); 
        vertices[1] = new Vector3(54.5f, 0, 2);
        vertices[2] = new Vector3(60.5f, 0, 2);
        vertices[3] = new Vector3(61, 0, 0);

        DrawObstacle(vertices, triangles);

        //water channel column 1-2
        vertices = new Vector3[4];
        vertices[0] = new Vector3(10.5f, 0, 74);
        vertices[1] = new Vector3(12, 0, 74);
        vertices[2] = new Vector3(18.2f, 0, 48.4f);
        vertices[3] = new Vector3(16.7f, 0, 48.4f);

        DrawObstacle(vertices, triangles);

        //water pier column 2
        vertices = new Vector3[4];
        vertices[0] = new Vector3(28.2f, 0, 82);
        vertices[1] = new Vector3(30.2f, 0, 83);
        vertices[2] = new Vector3(33.5f, 0, 75);
        vertices[3] = new Vector3(31.3f, 0, 74);

        DrawObstacle(vertices, triangles);

        //water pier column 2-2
        vertices = new Vector3[4];
        vertices[0] = new Vector3(33.5f, 0, 75);
        vertices[1] = new Vector3(33, 0, 77);
        vertices[2] = new Vector3(35, 0, 78);
        vertices[3] = new Vector3(36, 0, 76);

        DrawObstacle(vertices, triangles);
        */

        //draw just some Venice area
        //channel 1
        Vector3[] vertices = new Vector3[4];
        vertices[0] = new Vector3(80, 0, 99);
        vertices[1] = new Vector3(82f, 0, 100.2f);
        vertices[2] = new Vector3(83f, 0, 83);
        vertices[3] = new Vector3(81.5f, 0, 83);

        int[] triangles = new int[6];
        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 2;
        triangles[4] = 3;
        triangles[5] = 0;

        DrawObstacle(vertices, triangles);

        //channel 2
        /*vertices = new Vector3[4];
        vertices[0] = new Vector3(93.3f, 0, 100.2f);
        vertices[1] = new Vector3(95.7f, 0, 100.2f);
        vertices[2] = new Vector3(91, 0, 77.2f);
        vertices[3] = new Vector3(88.5f, 0, 77.8f);

        DrawObstacle(vertices, triangles);

        //pier 1
        vertices = new Vector3[4];
        vertices[0] = new Vector3(123f, 0, 93.3f);
        vertices[1] = new Vector3(124.3f, 0, 92.5f);
        vertices[2] = new Vector3(115.5f, 0, 78.8f);
        vertices[3] = new Vector3(114.2f, 0, 79.7f);

        DrawObstacle(vertices, triangles);

        //pier 1 turn
        vertices = new Vector3[4];
        vertices[0] = new Vector3(115.5f, 0, 78.8f);
        vertices[1] = new Vector3(115.7f, 0, 79.8f);
        vertices[2] = new Vector3(118.6f, 0, 78f);
        vertices[3] = new Vector3(117.6f, 0, 77.3f);

        DrawObstacle(vertices, triangles);*/

        //channel 2
        vertices = new Vector3[4];
        vertices[0] = new Vector3(93.3f, 0, 100.2f);
        vertices[1] = new Vector3(93.9f, 0, 100.2f);
        vertices[2] = new Vector3(89.5f, 0, 79.4522f);
        vertices[3] = new Vector3(88.885f, 0, 79.6f);

        DrawObstacle(vertices, triangles);

        //pier 1
        vertices = new Vector3[4];
        vertices[0] = new Vector3(123f, 0, 93.3f);
        vertices[1] = new Vector3(124.3f, 0, 92.5f);
        vertices[2] = new Vector3(115.5f, 0, 78.8f);
        vertices[3] = new Vector3(114.8f, 0, 79.7f);

        DrawObstacle(vertices, triangles);

        //pier 1 turn
        vertices = new Vector3[4];
        vertices[0] = new Vector3(115.5f, 0, 78.8f);
        vertices[1] = new Vector3(115.7f, 0, 79.8f);
        vertices[2] = new Vector3(118.6f, 0, 78f);
        vertices[3] = new Vector3(117.6f, 0, 77.3f);

        DrawObstacle(vertices, triangles);

        //pier 1 turn back
        vertices = new Vector3[4];
        vertices[0] = new Vector3(118.6f, 0, 78f);
        vertices[1] = new Vector3(117.6f, 0, 78.5f);
        vertices[2] = new Vector3(126.5f, 0, 85.4f);
        vertices[3] = new Vector3(126.5f, 0, 83.8f);

        DrawObstacle(vertices, triangles);

        //pier 1 back
        vertices = new Vector3[4];
        vertices[0] = new Vector3(127.2f, 0, 84.3f);
        vertices[1] = new Vector3(127.2f, 0, 85.9f);
        vertices[2] = new Vector3(130.7f, 0, 88.5f);
        vertices[3] = new Vector3(131.8f, 0, 87.5f);

        DrawObstacle(vertices, triangles);

        //channel bottom
        vertices = new Vector3[4];
        vertices[0] = new Vector3(117.6f, 0, 50.5f);
        vertices[1] = new Vector3(115.4f, 0, 49.5f);
        vertices[2] = new Vector3(101.9f, 0, 63.6f);
        vertices[3] = new Vector3(103.0f, 0, 64.7f);

        DrawObstacle(vertices, triangles);

        //channel left 2
        vertices = new Vector3[4];
        vertices[0] = new Vector3(76.6f, 0, 57.1f);
        vertices[1] = new Vector3(75.4f, 0, 59.4f);
        vertices[2] = new Vector3(84.8f, 0, 64.8f);
        vertices[3] = new Vector3(86.1f, 0, 62.5f);

        DrawObstacle(vertices, triangles);

        //channel left 1
        vertices = new Vector3[4];
        vertices[0] = new Vector3(68.6f, 0, 71.2f);
        vertices[1] = new Vector3(67.6f, 0, 73f);
        vertices[2] = new Vector3(71.4f, 0, 74.8f);
        vertices[3] = new Vector3(71.3f, 0, 72.5f);

        DrawObstacle(vertices, triangles);

        //channel left 1 inside
        vertices = new Vector3[4];
        vertices[0] = new Vector3(73.2f, 0, 73.5f);
        vertices[1] = new Vector3(73.2f, 0, 75.4f);
        vertices[2] = new Vector3(79.3f, 0, 78.2f);
        vertices[3] = new Vector3(80.1f, 0, 77);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(80.1f, 0, 77);
        vertices[1] = new Vector3(79.3f, 0, 78.2f);
        vertices[2] = new Vector3(81.6f, 0, 81.5f);
        vertices[3] = new Vector3(83.1f, 0, 81.4f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(83.1f, 0, 81.4f);
        vertices[1] = new Vector3(81.6f, 0, 81.5f);
        vertices[2] = new Vector3(81.553f, 0, 82.345f);
        vertices[3] = new Vector3(83.065f, 0, 82.345f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(81.442f, 0, 79.442f);
        vertices[1] = new Vector3(82.185f, 0, 80.523f);
        vertices[2] = new Vector3(84.966f, 0, 77.619f);
        vertices[3] = new Vector3(83.193f, 0, 77.619f);

        DrawObstacle(vertices, triangles);

        //channel left 1 inside
        vertices = new Vector3[4];
        vertices[0] = new Vector3(73.2f, 0, 73.5f);
        vertices[1] = new Vector3(73.2f, 0, 75.4f);
        vertices[2] = new Vector3(79.3f, 0, 78.2f);
        vertices[3] = new Vector3(80.1f, 0, 77);

        DrawObstacle(vertices, triangles);

        //channel 2 inside
        vertices = new Vector3[4];
        vertices[0] = new Vector3(84.746f, 0, 75.99f);
        vertices[1] = new Vector3(86.519f, 0, 75.99f);
        vertices[2] = new Vector3(87.974f, 0, 74.464f);
        vertices[3] = new Vector3(87.668f, 0, 72.927f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(88.279f, 0, 75.99f);
        vertices[1] = new Vector3(90.554f, 0, 75.537f);
        vertices[2] = new Vector3(88.121f, 0, 63.593f);
        vertices[3] = new Vector3(86.173f, 0, 65.421f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(89.861f, 0, 72.155f);
        vertices[1] = new Vector3(90.201f, 0, 73.815f);
        vertices[2] = new Vector3(98.607f, 0, 69.101f);
        vertices[3] = new Vector3(98.607f, 0, 66.214f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(98.607f, 0, 66.214f);
        vertices[1] = new Vector3(98.607f, 0, 69.101f);
        vertices[2] = new Vector3(101.978f, 0, 65.787f);
        vertices[3] = new Vector3(101.078f, 0, 64.534f);

        DrawObstacle(vertices, triangles);

        //lock everything out the defined area
        //left
        vertices = new Vector3[4];
        vertices[0] = new Vector3(0, 0, 0);
        vertices[1] = new Vector3(0, 0, 116);
        vertices[2] = new Vector3(60.718f, 0, 116);
        vertices[3] = new Vector3(60.718f, 0, 0);

        DrawObstacle(vertices, triangles);

        //right
        vertices = new Vector3[4];
        vertices[0] = new Vector3(137.235f, 0, 0);
        vertices[1] = new Vector3(137.235f, 0, 116);
        vertices[2] = new Vector3(158, 0, 116);
        vertices[3] = new Vector3(158, 0, 0);

        DrawObstacle(vertices, triangles);

        //up
        vertices = new Vector3[4];
        vertices[0] = new Vector3(0, 0, 100.087f);
        vertices[1] = new Vector3(0, 0, 116);
        vertices[2] = new Vector3(158, 0, 116);
        vertices[3] = new Vector3(158, 0, 100.087f);

        DrawObstacle(vertices, triangles);

        //down
        vertices = new Vector3[4];
        vertices[0] = new Vector3(0, 0, 0);
        vertices[1] = new Vector3(0, 0, 38.577f);
        vertices[2] = new Vector3(158, 0, 38.577f);
        vertices[3] = new Vector3(158, 0, 0);

        DrawObstacle(vertices, triangles);

        //surrounding water area
        vertices = new Vector3[4];
        vertices[0] = new Vector3(112.24f, 0, 100.087f);
        vertices[1] = new Vector3(113.639f, 0, 107.6195f);
        vertices[2] = new Vector3(137.669f, 0, 91.107f);
        vertices[3] = new Vector3(131.6575f, 0, 87.5985f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(131.6575f, 0, 87.5985f);
        vertices[1] = new Vector3(137.669f, 0, 91.107f);
        vertices[2] = new Vector3(142.97f, 0, 66.6375f);
        vertices[3] = new Vector3(137.234f, 0, 66.6375f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(137.234f, 0, 66.6375f);
        vertices[1] = new Vector3(142.97f, 0, 66.6375f);
        vertices[2] = new Vector3(137.091f, 0, 52.567f);
        vertices[3] = new Vector3(133.3335f, 0, 57.5408f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(133.3335f, 0, 57.5408f);
        vertices[1] = new Vector3(137.091f, 0, 52.567f);
        vertices[2] = new Vector3(94.0677f, 0, 31.5362f);
        vertices[3] = new Vector3(89.059f, 0, 37.565f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(91.3035f, 0, 38.5775f);
        vertices[1] = new Vector3(89.059f, 0, 37.565f);
        vertices[2] = new Vector3(80.257f, 0, 46.031f);
        vertices[3] = new Vector3(81.391f, 0, 48.719f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(81.391f, 0, 48.719f);
        vertices[1] = new Vector3(80.257f, 0, 46.031f);
        vertices[2] = new Vector3(62.72f, 0, 76.209f);
        vertices[3] = new Vector3(65.59f, 0, 76.64f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(65.59f, 0, 76.64f);
        vertices[1] = new Vector3(62.72f, 0, 76.209f);
        vertices[2] = new Vector3(58.176f, 0, 92.374f);
        vertices[3] = new Vector3(60.7187f, 0, 93.1255f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(124.59f, 0, 100.096f);
        vertices[1] = new Vector3(137.2408f, 0, 100.096f);
        vertices[2] = new Vector3(137.2408f, 0, 91.326f);
        vertices[3] = new Vector3(137.2408f, 0, 90.326f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(137.252f, 0, 52.9633f);
        vertices[1] = new Vector3(137.252f, 0, 38.39f);
        vertices[2] = new Vector3(106.32f, 0, 38.39f);
        vertices[3] = new Vector3(105.32f, 0, 38.39f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(88.261f, 0, 38.39f);
        vertices[1] = new Vector3(60.654f, 0, 38.39f);
        vertices[2] = new Vector3(60.654f, 0, 84.5f);
        vertices[3] = new Vector3(60.654f, 0, 85.5f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(60.683f, 0, 93.103f);
        vertices[1] = new Vector3(60.683f, 0, 100.114f);
        vertices[2] = new Vector3(81.852f, 0, 100.114f);
        vertices[3] = new Vector3(80.046f, 0, 98.964f);

        DrawObstacle(vertices, triangles);

        //plaza buildings
        vertices = new Vector3[4];
        vertices[0] = new Vector3(94.638f, 0, 90.958f);
        vertices[1] = new Vector3(98.652f, 0, 90.3f);
        vertices[2] = new Vector3(99.794f, 0, 88.474f);
        vertices[3] = new Vector3(94.131f, 0, 88.474f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(94.131f, 0, 88.474f);
        vertices[1] = new Vector3(99.794f, 0, 88.474f);
        vertices[2] = new Vector3(96.143f, 0, 71.542f);
        vertices[3] = new Vector3(91.244f, 0, 74.29f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(96.143f, 0, 71.542f);
        vertices[1] = new Vector3(97.223f, 0, 77.787f);
        vertices[2] = new Vector3(105.739f, 0, 75.737f);
        vertices[3] = new Vector3(99.164f, 0, 69.847f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(99.164f, 0, 69.847f);
        vertices[1] = new Vector3(105.739f, 0, 75.737f);
        vertices[2] = new Vector3(111.323f, 0, 69.632f);
        vertices[3] = new Vector3(108.484f, 0, 67.784f);

        DrawObstacle(vertices, triangles);

        //PROBLEMA: ESSE VERTICE!!
        vertices = new Vector3[4];
        vertices[0] = new Vector3(111.323f, 0, 69.632f); 
        vertices[1] = new Vector3(105.739f, 0, 75.737f);
        vertices[2] = new Vector3(112.819f, 0, 75.344f);
        vertices[3] = new Vector3(111.323f, 0, 69.632f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(112.819f, 0, 75.344f); 
        vertices[1] = new Vector3(105.739f, 0, 75.737f);
        vertices[2] = new Vector3(108.324f, 0, 86.409f);
        vertices[3] = new Vector3(115.345f, 0, 84.99f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(115.345f, 0, 84.99f);
        vertices[1] = new Vector3(108.324f, 0, 86.409f);
        vertices[2] = new Vector3(109.4267f, 0, 87.708f);
        vertices[3] = new Vector3(115.664f, 0, 86.2075f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(116.768f, 0, 85.942f);
        vertices[1] = new Vector3(109.4267f, 0, 87.708f);
        vertices[2] = new Vector3(110.003f, 0, 90.102f);
        vertices[3] = new Vector3(117.344f, 0, 88.336f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(113.201f, 0, 89.333f);
        vertices[1] = new Vector3(110.003f, 0, 90.102f);
        vertices[2] = new Vector3(110.8473f, 0, 94.2505f);
        vertices[3] = new Vector3(113.9222f, 0, 93.9015f);

        DrawObstacle(vertices, triangles);

        //island constructions (4)
        vertices = new Vector3[4];
        vertices[0] = new Vector3(133.6872f, 0, 76.3742f);
        vertices[1] = new Vector3(119.363f, 0, 68.081f);
        vertices[2] = new Vector3(117.8578f, 0, 76.1093f);
        vertices[3] = new Vector3(131.1162f, 0, 86.039f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(127.89f, 0, 71.608f);
        vertices[1] = new Vector3(134.0097f, 0, 75.1638f);
        vertices[2] = new Vector3(136.2586f, 0, 66.708f);
        vertices[3] = new Vector3(132.626f, 0, 58.235f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(127.89f, 0, 71.608f);
        vertices[1] = new Vector3(132.626f, 0, 58.235f);
        vertices[2] = new Vector3(131.6968f, 0, 57.816f);

        triangles = new int[3];
        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(130.9138f, 0, 57.4625f);
        vertices[1] = new Vector3(125.2113f, 0, 54.89f);
        vertices[2] = new Vector3(119.863f, 0, 66.944f);
        vertices[3] = new Vector3(126.852f, 0, 71.005f);

        triangles = new int[6];
        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 2;
        triangles[4] = 3;
        triangles[5] = 0;

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(108.481f, 0, 60.695f);
        vertices[1] = new Vector3(118.2975f, 0, 68.0715f);
        vertices[2] = new Vector3(124.408f, 0, 54.5275f);
        vertices[3] = new Vector3(117.7956f, 0, 51.544f);

        DrawObstacle(vertices, triangles);

        //bottom left island constructions (4)
        vertices = new Vector3[4];
        vertices[0] = new Vector3(88.9428f, 0, 62.9963f);
        vertices[1] = new Vector3(90.4935f, 0, 70.6092f);
        vertices[2] = new Vector3(97.3275f, 0, 65.9655f);
        vertices[3] = new Vector3(97.3275f, 0, 60.1907f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(97.3275f, 0, 60.1907f);
        vertices[1] = new Vector3(97.3275f, 0, 65.9655f);
        vertices[2] = new Vector3(100.478f, 0, 63.824f);
        vertices[3] = new Vector3(99.2627f, 0, 60.1907f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(100.062f, 0, 58.958f);
        vertices[1] = new Vector3(101.365f, 0, 62.897f);
        vertices[2] = new Vector3(113.8985f, 0, 49.7858f);
        vertices[3] = new Vector3(107.7876f, 0, 47.0286f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(105.671f, 0, 46.0738f);
        vertices[1] = new Vector3(91.51265f, 0, 39.686f);
        vertices[2] = new Vector3(84.969f, 0, 46.381f);
        vertices[3] = new Vector3(99.6895f, 0, 55.309f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(82.1356f, 0, 49.28f);
        vertices[1] = new Vector3(77.908f, 0, 56.7508f);
        vertices[2] = new Vector3(87.4198f, 0, 62.134f);
        vertices[3] = new Vector3(95.125f, 0, 58.9572f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(82.1356f, 0, 49.28f);
        vertices[1] = new Vector3(97.327f, 0, 58.9571f);
        vertices[2] = new Vector3(99.025f, 0, 56.3351f);
        vertices[3] = new Vector3(84.2174f, 0, 47.1502f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(95.125f, 0, 58.9572f);
        vertices[1] = new Vector3(97.327f, 0, 58.9571f);
        vertices[2] = new Vector3(82.1356f, 0, 49.28f);
        vertices[3] = new Vector3(82.1356f, 0, 49.281f);

        DrawObstacle(vertices, triangles);

        //middle left island constructions (4)
        vertices = new Vector3[4];
        vertices[0] = new Vector3(78.0055f, 0, 61.8615f);
        vertices[1] = new Vector3(75.741f, 0, 60.58f);
        vertices[2] = new Vector3(72.4905f, 0, 66.323f);
        vertices[3] = new Vector3(77.294f, 0, 67.2602f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(77.294f, 0, 67.2602f);
        vertices[1] = new Vector3(72.4905f, 0, 66.323f);
        vertices[2] = new Vector3(69.9578f, 0, 70.7985f);
        vertices[3] = new Vector3(74.004f, 0, 72.881f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(77.294f, 0, 67.2602f);
        vertices[1] = new Vector3(74.004f, 0, 72.881f);
        vertices[2] = new Vector3(78.7446f, 0, 75.3212f);
        vertices[3] = new Vector3(81.1275f, 0, 69.234f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(81.3688f, 0, 68.617f);
        vertices[1] = new Vector3(82.9235f, 0, 64.645f);
        vertices[2] = new Vector3(79.867f, 0, 62.915f);
        vertices[3] = new Vector3(79.867f, 0, 67.8645f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(79.867f, 0, 67.8645f);
        vertices[1] = new Vector3(79.867f, 0, 62.915f);
        vertices[2] = new Vector3(78.6445f, 0, 62.915f);
        vertices[3] = new Vector3(78.0061f, 0, 66.9318f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(85.348f, 0, 66.017f);
        vertices[1] = new Vector3(83.44f, 0, 64.937f);
        vertices[2] = new Vector3(81.8915f, 0, 68.9945f);
        vertices[3] = new Vector3(85.6588f, 0, 70.8585f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(85.348f, 0, 66.017f);
        vertices[1] = new Vector3(85.6588f, 0, 70.8585f);
        vertices[2] = new Vector3(86.2262f, 0, 70.4234f);
        vertices[3] = new Vector3(85.348f, 0, 66.017f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(82.3143f, 0, 69.9265f);
        vertices[1] = new Vector3(81.395f, 0, 70.295f);
        vertices[2] = new Vector3(79.3565f, 0, 75.63645f);
        vertices[3] = new Vector3(80.785f, 0, 76.371f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(81.7563f, 0, 77.7823f);
        vertices[1] = new Vector3(86.378f, 0, 72.938f);
        vertices[2] = new Vector3(86.521f, 0, 71.933f);
        vertices[3] = new Vector3(80.785f, 0, 76.371f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(86.521f, 0, 71.933f);
        vertices[1] = new Vector3(82.3143f, 0, 69.9265f);
        vertices[2] = new Vector3(80.785f, 0, 76.371f);
        vertices[3] = new Vector3(80.785f, 0, 76.371f);

        DrawObstacle(vertices, triangles);

        //upper left island constructions (5)
        vertices = new Vector3[4];
        vertices[0] = new Vector3(61.8672f, 0, 92.5005f);
        vertices[1] = new Vector3(66.0986f, 0, 93.751f);
        vertices[2] = new Vector3(66.835f, 0, 93.159f);
        vertices[3] = new Vector3(63.4122f, 0, 87.2723f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(63.4122f, 0, 87.2723f);
        vertices[1] = new Vector3(66.835f, 0, 93.159f);
        vertices[2] = new Vector3(66.839f, 0, 88.285f);
        vertices[3] = new Vector3(63.4122f, 0, 87.2723f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(67.8158f, 0, 88.5735f);
        vertices[1] = new Vector3(67.8158f, 0, 94.2585f);
        vertices[2] = new Vector3(71.075f, 0, 95.22f);
        vertices[3] = new Vector3(71.075f, 0, 88.5735f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(71.57f, 0, 86.7145f);
        vertices[1] = new Vector3(71.57f, 0, 95.3678f);
        vertices[2] = new Vector3(79.3285f, 0, 97.66f);
        vertices[3] = new Vector3(80.386f, 0, 85.174f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(71.57f, 0, 86.7145f);
        vertices[1] = new Vector3(80.386f, 0, 85.174f);
        vertices[2] = new Vector3(72.865f, 0, 81.835f);
        vertices[3] = new Vector3(71.57f, 0, 86.7145f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(72.982f, 0, 81.3903f);
        vertices[1] = new Vector3(80.433f, 0, 84.598f);
        vertices[2] = new Vector3(80.6775f, 0, 81.736f);
        vertices[3] = new Vector3(73.883f, 0, 77.9915f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(73.883f, 0, 77.9915f);
        vertices[1] = new Vector3(80.6775f, 0, 81.736f);
        vertices[2] = new Vector3(78.751f, 0, 78.997f);
        vertices[3] = new Vector3(75.194f, 0, 77.376f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(67.819f, 0, 87.931f);
        vertices[1] = new Vector3(70.2685f, 0, 87.931f);
        vertices[2] = new Vector3(73.4135f, 0, 76.5587f);
        vertices[3] = new Vector3(68.079f, 0, 74.124f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(67.819f, 0, 87.931f);
        vertices[1] = new Vector3(68.079f, 0, 74.124f);
        vertices[2] = new Vector3(66.4463f, 0, 77.0026f);
        vertices[3] = new Vector3(63.586f, 0, 86.6828f);

        DrawObstacle(vertices, triangles);

        //upper middle island constructions (5)
        vertices = new Vector3[4];
        vertices[0] = new Vector3(84.4082f, 0, 99.1612f);
        vertices[1] = new Vector3(91.9556f, 0, 99.1612f);
        vertices[2] = new Vector3(91.1465f, 0, 95.1034f);
        vertices[3] = new Vector3(88.57f, 0, 95.1034f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(84.4082f, 0, 99.1612f);
        vertices[1] = new Vector3(88.57f, 0, 95.1034f);
        vertices[2] = new Vector3(83.9361f, 0, 93.3695f);
        vertices[3] = new Vector3(83.2291f, 0, 93.8255f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(84.4082f, 0, 99.1612f);
        vertices[1] = new Vector3(83.2291f, 0, 93.8255f);
        vertices[2] = new Vector3(82.905f, 0, 98.717f);
        vertices[3] = new Vector3(84.4082f, 0, 99.1612f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(83.317f, 0, 92.5077f);
        vertices[1] = new Vector3(86.2607f, 0, 93.1213f);
        vertices[2] = new Vector3(86.5735f, 0, 91.623f);
        vertices[3] = new Vector3(86.5735f, 0, 88.107f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(83.317f, 0, 92.5077f);
        vertices[1] = new Vector3(86.5735f, 0, 88.107f);
        vertices[2] = new Vector3(83.6213f, 0, 87.911f);
        vertices[3] = new Vector3(83.317f, 0, 92.5077f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(87.2698f, 0, 88.153f);
        vertices[1] = new Vector3(87.2698f, 0, 93.1218f);
        vertices[2] = new Vector3(88.572f, 0, 94.0815f);
        vertices[3] = new Vector3(90.943f, 0, 94.0815f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(87.2698f, 0, 88.153f);
        vertices[1] = new Vector3(90.943f, 0, 94.0815f);
        vertices[2] = new Vector3(89.7942f, 0, 88.3202f);
        vertices[3] = new Vector3(87.2698f, 0, 88.153f); 

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(83.6878f, 0, 86.9098f);
        vertices[1] = new Vector3(89.591f, 0, 87.301f);
        vertices[2] = new Vector3(88.798f, 0, 83.3215f);
        vertices[3] = new Vector3(83.947f, 0, 83);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(84.005f, 0, 82.1227f);
        vertices[1] = new Vector3(87.5295f, 0, 82.3561f);
        vertices[2] = new Vector3(88.4062f, 0, 81.3572f);
        vertices[3] = new Vector3(84.071f, 0, 81.142f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(84.071f, 0, 81.142f);
        vertices[1] = new Vector3(88.4062f, 0, 81.3572f);
        vertices[2] = new Vector3(87.567f, 0, 77.1467f);
        vertices[3] = new Vector3(86.744f, 0, 77.092f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(84.071f, 0, 81.142f);
        vertices[1] = new Vector3(86.744f, 0, 77.092f);
        vertices[2] = new Vector3(83.82055f, 0, 80.1546f);
        vertices[3] = new Vector3(83.82055f, 0, 80.7778f);

        DrawObstacle(vertices, triangles);

        //frufrus
        vertices = new Vector3[4];
        vertices[0] = new Vector3(102.22f, 0, 81.355f);
        vertices[1] = new Vector3(102.559f, 0, 82.621f);
        vertices[2] = new Vector3(103.825f, 0, 82.2818f);
        vertices[3] = new Vector3(103.4857f, 0, 81.016f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(106.422f, 0, 86.551f);
        vertices[1] = new Vector3(106.422f, 0, 87.5991f);
        vertices[2] = new Vector3(107.4972f, 0, 87.5991f);
        vertices[3] = new Vector3(107.4972f, 0, 86.551f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(100.408f, 0, 88.802f);
        vertices[1] = new Vector3(100.408f, 0, 89.88f);
        vertices[2] = new Vector3(101.435f, 0, 89.88f);
        vertices[3] = new Vector3(101.435f, 0, 88.802f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(101.405f, 0, 88.792f);
        vertices[1] = new Vector3(101.405f, 0, 89.85f);
        vertices[2] = new Vector3(102.433f, 0, 89.85f);
        vertices[3] = new Vector3(102.433f, 0, 88.792f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(100.419f, 0, 90.527f);
        vertices[1] = new Vector3(100.419f, 0, 91.587f);
        vertices[2] = new Vector3(101.448f, 0, 91.587f);
        vertices[3] = new Vector3(101.448f, 0, 90.527f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(101.54f, 0, 90.627f);
        vertices[1] = new Vector3(101.54f, 0, 91.689f);
        vertices[2] = new Vector3(102.564f, 0, 91.689f);
        vertices[3] = new Vector3(102.564f, 0, 90.627f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(106.78f, 0, 89.04f);
        vertices[1] = new Vector3(106.78f, 0, 90.1f);
        vertices[2] = new Vector3(107.807f, 0, 90.1f);
        vertices[3] = new Vector3(107.807f, 0, 89.04f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(107.97f, 0, 89.255f);
        vertices[1] = new Vector3(107.97f, 0, 90.314f);
        vertices[2] = new Vector3(108.995f, 0, 90.314f);
        vertices[3] = new Vector3(108.995f, 0, 89.255f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(106.954f, 0, 91.342f);
        vertices[1] = new Vector3(106.954f, 0, 92.401f);
        vertices[2] = new Vector3(107.987f, 0, 92.401f);
        vertices[3] = new Vector3(107.987f, 0, 91.342f);

        DrawObstacle(vertices, triangles);

        vertices = new Vector3[4];
        vertices[0] = new Vector3(108.03f, 0, 91.272f);
        vertices[1] = new Vector3(108.03f, 0, 92.331f);
        vertices[2] = new Vector3(109.062f, 0, 92.331f);
        vertices[3] = new Vector3(109.062f, 0, 91.272f);

        DrawObstacle(vertices, triangles);
    }

    //generate the metric between number of signs and time
    //unused!
    /*public void GenerateMetric() {
        //get all subdirectories within the defined config directory
        allDirs = Directory.GetDirectories(Application.dataPath + "/" + allSimulations);

        //for each sim directory
        foreach (string dir in allDirs)
        {
            //we take the AgentsGoal file for information
            StreamReader theReader = new StreamReader(dir + "/AgentsGoal.csv", System.Text.Encoding.Default);
            float timeCount = 0;
            int lineCount = 0;
            string line;
            
            using (theReader)
            {
                do
                {
                    //we get the updated simulation Index
                    line = theReader.ReadLine();
                    if (line != null && line != "")
                    {
                        string[] info = line.Split(';');
                        //sums up the arrival times
                        timeCount += System.Int32.Parse(info[2]);
                        lineCount++;
                    }
                } while (line != null);
            }
            theReader.Close();

            //now, we take the signs file for information about how many signs were
            theReader = new StreamReader(dir + "/signs.dat", System.Text.Encoding.Default);
            int qntSigns = 0;

            using (theReader)
            {
                do
                {
                    //we get the updated simulation Index
                    line = theReader.ReadLine();
                    if (line != null && line != "")
                    {
                        qntSigns = System.Int32.Parse(line);
                        //just need to read the first line, which has the qnt signs
                        break;
                    }
                } while (line != null);
            }
            theReader.Close();

            //mean time
            timeCount /= lineCount;

            //open metric file to save info each frame
            StreamWriter metricFile = File.CreateText(dir + "/metric.csv");
            metricFile.WriteLine("Mean Time: "+timeCount);
            metricFile.WriteLine("Qnt Signs: " + qntSigns);
            //TODO: DEFINE FORMULA
            metricFile.WriteLine("Metric value: " + (timeCount * qntSigns));
            metricFile.Close();
        }
    }*/

    //load the obstacle file
    public void LoadObstacles()
    {
        StreamReader theReader = new StreamReader(Application.dataPath + "/" + obstaclesFilename, System.Text.Encoding.Default);
        string line;
        int qntObstacles = 0;
        int qntVertices = 0;
        int qntTriangles = 0;
        Vector3[] vertices = new Vector3[qntVertices];
        int[] triangles = new int[qntTriangles];

        using (theReader)
        {
            int lineCount = 1;
            // While there's lines left in the text file, do this:
            do
            {
                line = theReader.ReadLine();

                if (line != null)
                {
                    //line 1 = qntObstacles
                    if(lineCount == 1)
                    {
                        string[] info = line.Split(':');
                        qntObstacles = System.Int32.Parse(info[1]);
                    }//else, if the line is "Obstacle", it is a new obstacle, so reset vertices
                    else if(line == "Obstacle")
                    {
                        //reset vertices
                        vertices = new Vector3[0];
                        triangles = new int[0];
                    }//else, if line contains "qntVertices", set it and start to read the vertices
                    else if (line.Contains("qntVertices"))
                    {
                        string[] info = line.Split(':');
                        qntVertices = System.Int32.Parse(info[1]);
                        vertices = new Vector3[qntVertices];
                    }//else, if line contains "qntTriangles", set it and start to read the triangles
                    else if (line.Contains("qntTriangles"))
                    {
                        string[] info = line.Split(':');
                        qntTriangles = System.Int32.Parse(info[1]);
                        triangles = new int[qntTriangles];
                    }//else, read the information
                    else
                    {
                        //if qntVertices is bigger than 0, reading vertices yet
                        if(qntVertices > 0)
                        {
                            string[] info = line.Split(';');
                            vertices[qntVertices - 1] = new Vector3(System.Convert.ToSingle(info[0]), System.Convert.ToSingle(info[1]), System.Convert.ToSingle(info[2]));

                            //decrement
                            qntVertices--;
                        }//else, we are already reading the triangles
                        else if (qntTriangles > 0)
                        {
                            triangles[qntTriangles - 1] = System.Int32.Parse(line);

                            //decrement
                            qntTriangles--;

                            //if reached 0, obstacle is ready to draw
                            if(qntTriangles == 0)
                            {
                                DrawObstacle(vertices, triangles);
                            }
                        }
                    }
                }

                lineCount++;
            }
            while (line != null);
        }
        //close file
        theReader.Close();
    }

    //load cells and auxins and obstacles and goals (static stuff)
    //this method is invoked on GameController Pre-Process, define on EditorController
    public void LoadCellsAuxins()
    {
        string line;

        //ReadOBJFile();
        LoadObstacles();

        // Create a new StreamReader, tell it which file to read and what encoding the file
        StreamReader theReader = new StreamReader(Application.dataPath + Path.DirectorySeparatorChar + configFilename, System.Text.Encoding.Default);

        //parents
        GameObject parentCells = GameObject.Find("Cells");

        int qntCells = 0;
        // Create a new StreamReader, tell it which file to read and what encoding the file
        theReader = new StreamReader(Path.Combine(Application.dataPath, configFilename), System.Text.Encoding.Default);
        Terrain terrain = GameObject.Find("Terrain").GetComponent<Terrain>();

        using (theReader)
        {
            int lineCount = 1;
            // While there's lines left in the text file, do this:
            do
            {
                line = theReader.ReadLine();

                if (line != null)
                {
                    //in first line, we have the terrain size
                    if (lineCount == 1)
                    {
                        string[] entries = line.Split(':');
                        entries = entries[1].Split(',');

                        scenarioSizeX = System.Convert.ToSingle(entries[0]);
                        scenarioSizeZ = System.Convert.ToSingle(entries[1]);

                        terrain.terrainData.size = new Vector3(scenarioSizeX, terrain.terrainData.size.y, scenarioSizeZ);
                    }
                    //in second line, we have the camera position
                    else if (lineCount == 2)
                    {
                        ConfigCamera();
                    }
                    //in the third line, we have the qntCells to instantiante
                    else if (lineCount == 3)
                    {
                        string[] entries = line.Split(':');
                        qntCells = System.Int32.Parse(entries[1]);
                    }
                    //else, if we are in the qntCells+4 line, we have the qntAuxins to instantiante
                    else if (lineCount == qntCells + 4)
                    {
                        string[] entries = line.Split(':');
                        qntAuxins = System.Int32.Parse(entries[1]);
                    }
                    else
                    {
                        //while we are til qntCells+3 line, we have cells. After that, we have auxins and then, agents
                        if (lineCount <= qntCells + 3)
                        {
                            string[] entries = line.Split(';');

                            if (entries.Length > 0)
                            {
                                GameObject newCell = Instantiate(cell, new Vector3(System.Convert.ToSingle(entries[1]), System.Convert.ToSingle(entries[2]), System.Convert.ToSingle(entries[3])),
                                    Quaternion.identity) as GameObject;
                                //change scale
                                newCell.transform.localScale *= System.Convert.ToSingle(entries[4]);
                                cellRadius = System.Convert.ToSingle(entries[4]);
                                //change his name
                                newCell.name = entries[0];
                                //change parent
                                newCell.transform.parent = parentCells.transform;
                            }
                        }
                        else if (lineCount <= qntCells + qntAuxins + 4)
                        {
                            string[] entries = line.Split(';');
                            if (entries.Length > 0)
                            {
                                //find his cell
                                GameObject hisCell = GameObject.Find(entries[5]);

                                AuxinController newAuxin = new AuxinController();
                                //change his name
                                newAuxin.name = entries[0];
                                //this auxin is from this cell
                                newAuxin.SetCell(hisCell);
                                //set position
                                newAuxin.position = new Vector3(System.Convert.ToSingle(entries[1]), System.Convert.ToSingle(entries[2]), System.Convert.ToSingle(entries[3]));
                                //alter auxinRadius
                                auxinRadius = System.Convert.ToSingle(entries[4]);
                                //add this auxin to this cell
                                hisCell.GetComponent<CellController>().AddAuxin(newAuxin);
                            }
                        }
                    }
                }
                lineCount++;
            }
            while (line != null);
            // Done reading, close the reader and return true to broadcast success    
            theReader.Close();
        }

        // Create a new StreamReader, tell it which file to read and what encoding the file
        //Goals file, with goals and their positions
        theReader = new StreamReader(Path.Combine(Application.dataPath, goalsFilename), System.Text.Encoding.Default);

        using (theReader)
        {
            int lineCount = 1;
            // While there's lines left in the text file, do this:
            do
            {
                line = theReader.ReadLine();

                if (line != null)
                {
                    //in first line, it is the qnt goals
                    if (lineCount == 1)
                    {
                        qntGoals = System.Int32.Parse(line);
                    }
                    else
                    {
                        //each line 1 agent, separated by " "
                        string[] entries = line.Split(' ');

                        //goal position
                        Vector3 newPosition = new Vector3(System.Convert.ToSingle(entries[1]), goalP.transform.position.y, System.Convert.ToSingle(entries[2]));

                        //instantiante it
                        DrawGoal(entries[0], newPosition);
                    }
                }
                lineCount++;
            }
            while (line != null);
            // Done reading, close the reader and return true to broadcast success    
            theReader.Close();
        }
    }

    //check group vertices to find the corners
    //unused!
    /*public void CheckGroupVertices()
    {
        allObstacles = GameObject.FindGameObjectsWithTag("Obstacle");
        //first obstacle, vertices and triangles
        Vector3[] vertices = allObstacles[0].GetComponent<MeshFilter>().sharedMesh.vertices;
        int[] triangles = allObstacles[0].GetComponent<MeshFilter>().sharedMesh.triangles;

        int[] groupTriangles = new int[triangles.Length];
        //start each index with 0
        for (int i = 0; i < groupTriangles.Length; i++)
        {
            groupTriangles[i] = 0;
        }

        int group = 0;

        float elapsed = 0f;

        //go through the triangles checking if they are related each other
        while (true)
        {
            bool die = true;
            //int ind = 0;

            //check if we need to update group value
            for (int i = 0; i < groupTriangles.Length; i = i + 3)
            {
                //if there is at least one zero value yet, need to keep going
                if (groupTriangles[i] == 0)
                {
                    die = false;

                    //update group
                    group++;

                    //initial values for new group
                    groupTriangles[i] = group;
                    groupTriangles[i + 1] = group;
                    groupTriangles[i + 2] = group;

                    break;
                }
            }

            //if there are no more zero values, or time is up, break;
            if (die || elapsed > 600)
            {
                Debug.Log(elapsed);
                break;
            }

            //for each triangle, we get it...
            for (int i = 0; i < triangles.Length; i = i + 3)
            {
                int triangleI1 = triangles[i];
                int triangleI2 = triangles[i + 1];
                int triangleI3 = triangles[i + 2];

                //...to compare with other triangles
                for (int j = 0; j < triangles.Length; j = j + 3)
                {
                    if (i != j)
                    {
                        int triangleJ1 = triangles[j];
                        int triangleJ2 = triangles[j + 1];
                        int triangleJ3 = triangles[j + 2];

                        //super if to see if the 2 triangles have any vertice in common
                        if (triangleJ1 == triangleI1 || triangleJ1 == triangleI2 || triangleJ1 == triangleI3 ||
                            triangleJ2 == triangleI1 || triangleJ2 == triangleI2 || triangleJ2 == triangleI3 ||
                            triangleJ3 == triangleI1 || triangleJ3 == triangleI2 || triangleJ3 == triangleI3)
                        {
                            //if they do, they are from the same group!
                            //now, need to see which group it is
                            if (groupTriangles[i] > 0 && (groupTriangles[j] == 0 && groupTriangles[j + 1] == 0 && groupTriangles[j + 2] == 0))
                            {
                                groupTriangles[j] = groupTriangles[i];
                                groupTriangles[j + 1] = groupTriangles[i];
                                groupTriangles[j + 2] = groupTriangles[i];
                            }
                            else if (groupTriangles[j] > 0 && (groupTriangles[i] == 0 && groupTriangles[i + 1] == 0 && groupTriangles[i + 2] == 0))
                            {
                                groupTriangles[i] = groupTriangles[j];
                                groupTriangles[i + 1] = groupTriangles[j];
                                groupTriangles[i + 2] = groupTriangles[j];
                            }
                        }
                    }
                }
            }

            elapsed += Time.deltaTime;
        }

        //Debug.Log(group);

        //start the newVertices with group * 4 sides
        //IMPORTANT: always consider that each group forms a quadrilateral (4 sides)
        Vector3[] newVertices = new Vector3[group * 4];
        int verticesIndex = 0;
        int groupIT = 1;
        while (groupIT <= group)
        {
            //the boundary
            float maxX = 0;
            float maxZ = 0;
            float minX = 0;
            float minZ = 0;

            bool first = true;
            //here, we get the boundary
            for (int i = 0; i < groupTriangles.Length; i++)
            {
                if (groupTriangles[i] == groupIT)
                {
                    //if first, initial values
                    if (first)
                    {
                        maxX = vertices[triangles[i]].x;
                        maxZ = vertices[triangles[i]].z;
                        minX = vertices[triangles[i]].x;
                        minZ = vertices[triangles[i]].z;
                        first = false;
                    }//else, compare with initial values to see if fits
                    else
                    {
                        if (vertices[triangles[i]].x > maxX)
                        {
                            maxX = vertices[triangles[i]].x;
                        }
                        if (vertices[triangles[i]].x < minX)
                        {
                            minX = vertices[triangles[i]].x;
                        }
                        if (vertices[triangles[i]].z > maxZ)
                        {
                            maxZ = vertices[triangles[i]].z;
                        }
                        if (vertices[triangles[i]].z < minZ)
                        {
                            minZ = vertices[triangles[i]].z;
                        }
                    }
                }
            }

            //lists for the vertices which belong the boundary
            List<Vector3> allMinX = new List<Vector3>();
            List<Vector3> allMaxX = new List<Vector3>();
            List<Vector3> allMinZ = new List<Vector3>();
            List<Vector3> allMaxZ = new List<Vector3>();
            for (int i = 0; i < groupTriangles.Length; i++)
            {
                if (groupTriangles[i] == groupIT)
                {
                    //new idea: find all vertices with minX, maxX, etc; and use them directly, with no bounding box
                    if (vertices[triangles[i]].x == minX)
                    {
                        //if it is not in the list yet, add
                        if (!allMinX.Contains(vertices[triangles[i]]))
                        {
                            allMinX.Add(vertices[triangles[i]]);
                        }
                    }
                    if (vertices[triangles[i]].x == maxX)
                    {
                        //if it is not in the list yet, add
                        if (!allMaxX.Contains(vertices[triangles[i]]))
                        {
                            allMaxX.Add(vertices[triangles[i]]);
                        }
                    }
                    if (vertices[triangles[i]].z == minZ)
                    {
                        //if it is not in the list yet, add
                        if (!allMinZ.Contains(vertices[triangles[i]]))
                        {
                            allMinZ.Add(vertices[triangles[i]]);
                        }
                    }
                    if (vertices[triangles[i]].z == maxZ)
                    {
                        //if it is not in the list yet, add
                        if (!allMaxZ.Contains(vertices[triangles[i]]))
                        {
                            allMaxZ.Add(vertices[triangles[i]]);
                        }
                    }
                }
            }

            List<Vector3> fourVertices = new List<Vector3>();
            //now we have the possible vertices. Lets decide which 4 to use
            //if allMinX just have 1 vertice, just check his z value
            if (fourVertices.Count < 4)
            {
                if (allMinX.Count == 1)
                {
                    if (!fourVertices.Contains(allMinX[0]))
                    {
                        fourVertices.Add(allMinX[0]);
                    }
                }
                //else, it must already contain bottom and top
                else
                {
                    //find the highest and lowest z
                    Vector3 lZ = new Vector3(1000f, 0, 1000f);
                    Vector3 hZ = new Vector3(-1000f, 0, -1000f);
                    foreach (Vector3 cont in allMinX)
                    {
                        if (cont.z < lZ.z)
                        {
                            lZ = cont;
                        }
                        if (cont.z > hZ.z)
                        {
                            hZ = cont;
                        }
                    }

                    if (!fourVertices.Contains(lZ))
                    {
                        fourVertices.Add(lZ);
                    }
                    if (!fourVertices.Contains(hZ))
                    {
                        fourVertices.Add(hZ);
                    }
                }
            }

            //if allMaxX just have 1 vertice, just check his z value
            if (fourVertices.Count < 4)
            {
                if (allMaxX.Count == 1)
                {
                    if (!fourVertices.Contains(allMaxX[0]))
                    {
                        fourVertices.Add(allMaxX[0]);
                    }
                }
                //else, it must already contain bottom and top
                else
                {
                    //find the highest and lowest z
                    Vector3 lZ = new Vector3(1000f, 0, 1000f);
                    Vector3 hZ = new Vector3(-1000f, 0, -1000f);
                    foreach (Vector3 cont in allMaxX)
                    {
                        if (cont.z < lZ.z)
                        {
                            lZ = cont;
                        }
                        if (cont.z > hZ.z)
                        {
                            hZ = cont;
                        }
                    }
                    if (!fourVertices.Contains(lZ))
                    {
                        fourVertices.Add(lZ);
                    }
                    if (!fourVertices.Contains(hZ))
                    {
                        fourVertices.Add(hZ);
                    }
                }
            }

            if (fourVertices.Count < 4)
            {
                //if allMinZ just have 1 vertice, just check his x value
                if (allMinZ.Count == 1)
                {
                    if (!fourVertices.Contains(allMinZ[0]))
                    {
                        fourVertices.Add(allMinZ[0]);
                    }
                }
                //else, it must already contain left and right
                else
                {
                    //find the highest and lowest z
                    Vector3 lX = new Vector3(1000f, 0, 1000f);
                    Vector3 hX = new Vector3(-1000f, 0, -1000f);
                    foreach (Vector3 cont in allMinZ)
                    {
                        if (cont.x < lX.x)
                        {
                            lX = cont;
                        }
                        if (cont.x > hX.x)
                        {
                            hX = cont;
                        }
                    }
                    if (!fourVertices.Contains(lX))
                    {
                        fourVertices.Add(lX);
                    }
                    if (!fourVertices.Contains(hX))
                    {
                        fourVertices.Add(hX);
                    }
                }
            }

            if (fourVertices.Count < 4)
            {
                //if allMaxZ just have 1 vertice, just check his x value
                if (allMaxZ.Count == 1)
                {
                    if (!fourVertices.Contains(allMaxZ[0]))
                    {
                        fourVertices.Add(allMaxZ[0]);
                    }
                }
                //else, it must already contain left and right
                else
                {
                    //find the highest and lowest z
                    Vector3 lX = new Vector3(1000f, 0, 1000f);
                    Vector3 hX = new Vector3(-1000f, 0, -1000f);
                    foreach (Vector3 cont in allMaxZ)
                    {
                        if (cont.x < lX.x)
                        {
                            lX = cont;
                        }
                        if (cont.x > hX.x)
                        {
                            hX = cont;
                        }
                    }
                    if (!fourVertices.Contains(lX))
                    {
                        fourVertices.Add(lX);
                    }
                    if (!fourVertices.Contains(hX))
                    {
                        fourVertices.Add(hX);
                    }
                }
            }

            //now, assign the values
            foreach (Vector3 four in fourVertices)
            {
                //newVertices[verticesIndex] = four + new Vector3(obstacleDisplacement, 0, obstacleDisplacement);
                Vector3 newVert = new Vector3(four.x/10, four.y, four.z/10);
                newVertices[verticesIndex] = newVert + new Vector3(obstacleDisplacement, 0, obstacleDisplacement);
                verticesIndex++;
            }

            groupIT++;
        }

        verticesObstacles = newVertices;
    }*/

    //Read the obstacle obj file
    public void ReadOBJFile()
    {
        StreamReader theReader = new StreamReader(Application.dataPath + "/" + obstaclesFilename, System.Text.Encoding.Default);
        string line;
        int qntVertices = 0;
        int qntTriangles = 0;
        Vector3[] vertices = new Vector3[qntVertices];
        int[] triangles = new int[qntTriangles];
        int controlVertice = 0;
        int controlTriangle = 0;

        using (theReader)
        {
            int lineCount = 1;
            // While there's lines left in the text file, do this:
            do
            {
                line = theReader.ReadLine();

                if (line != null)
                {
                    //if it contains #, it is a comment
                    if (line.Contains("#"))
                    {
                        if (line.Contains("vertices"))
                        {
                            string[] info = line.Split(' ');
                            qntVertices = System.Int32.Parse(info[1]);
                            vertices = new Vector3[qntVertices];
                        }
                        if (line.Contains("facets"))
                        {
                            string[] info = line.Split(' ');
                            qntTriangles = System.Int32.Parse(info[1]);
                            triangles = new int[qntTriangles * 3];
                        }
                    }
                    else if (line != "")
                    {
                        string[] entries = line.Split(' ');
                        //if it starts with v, it is vertice. else, if it starts with f, it is facet which form a triangle (hopefully!)
                        if (entries[0] == "v")
                        {
                            vertices[controlVertice] = new Vector3(System.Convert.ToSingle(entries[1]), System.Convert.ToSingle(entries[2]), System.Convert.ToSingle(entries[3]));
                            controlVertice++;
                        }
                        else if (entries[0] == "f")
                        {
                            triangles[controlTriangle] = System.Int32.Parse(entries[2]) - 1;
                            controlTriangle++;

                            triangles[controlTriangle] = System.Int32.Parse(entries[3]) - 1;
                            controlTriangle++;

                            triangles[controlTriangle] = System.Int32.Parse(entries[4]) - 1;
                            controlTriangle++;
                        }
                    }
                }

                lineCount++;
            }
            while (line != null);
        }
        //close file
        theReader.Close();

        DrawObstacle(vertices, triangles);
    }

    //save the new OBJ file
    public void SaveOBJFile()
    {
        //get all obstacles (which should already be on the scene, along cells, goals, signs and markers)
        allObstacles = GameObject.FindGameObjectsWithTag("Obstacle");

        Vector3[] vertices = allObstacles[0].GetComponent<MeshFilter>().sharedMesh.vertices;
        int[] triangles = allObstacles[0].GetComponent<MeshFilter>().sharedMesh.triangles;
        float scale = allObstacles[0].transform.localScale.x;

        StreamWriter obsFile = new StreamWriter(Application.dataPath + "/Obstacles.obj");
        obsFile.WriteLine("# file written from Unity BioCrowds simulation tool in Wavefront obj format");
        obsFile.WriteLine("# "+ vertices.Length + " vertices");
        obsFile.WriteLine("# 1010 halfedges (i dont actually have this info, so, just copied from original)");
        obsFile.WriteLine("# " + (triangles.Length/3) + " facets");

        obsFile.WriteLine("");

        obsFile.WriteLine("# " + vertices.Length + " vertices");
        obsFile.WriteLine("# ------------------------------------------");

        obsFile.WriteLine("");

        foreach (Vector3 vert in vertices)
        {
            obsFile.WriteLine("v " + (vert.x*scale) + " " + (vert.y*scale) + " " + (vert.z*scale));
        }

        obsFile.WriteLine("");

        obsFile.WriteLine("# " + (triangles.Length / 3) + " facets");
        obsFile.WriteLine("# ------------------------------------------");

        obsFile.WriteLine("");

        for(int i = 0; i < triangles.Length; i = i + 3)
        {
            obsFile.WriteLine("f  "+ (triangles[i]+1) + " " + (triangles[i+1]+1) + " " + (triangles[i+2]+1) + "");
        }

        obsFile.WriteLine("");

        obsFile.WriteLine("# End of Wavefront obj format #");

        obsFile.Close();
    }
    
    //load a csv config file
    private void LoadConfigFile()
    {
        string line;

        //parents
        GameObject parentAgents = GameObject.Find("Agents");

        // Create a new StreamReader, tell it which file to read and what encoding the file
        //schedule file, with agents and their schedules
        StreamReader theReader = new StreamReader(scheduleFilename, System.Text.Encoding.Default);

        using (theReader)
        {
            int lineCount = 1;
            // While there's lines left in the text file, do this:
            do
            {
                line = theReader.ReadLine();

                if (line != null)
                {
                    //in first line, it is the qnt agents
                    if (lineCount == 1)
                    {
                        qntAgents = System.Int32.Parse(line);
                    }
                    else
                    {
                        //each line 1 agent, separated by " "
                        string[] entries = line.Split(' ');

                        Vector3 newPosition = new Vector3(System.Convert.ToSingle(entries[0]), agent.transform.position.y, System.Convert.ToSingle(entries[1]));
                        float grain = 1f;
                        float originalGrain = grain;

                        //check if there is an obstacle in this position
                        while (CheckObstacle(newPosition, "Obstacle", 0.1f))
                        {
                            //if there is an obstacle, test with new positions
                            if (!CheckObstacle(newPosition + new Vector3(grain, 0f, 0f), "Obstacle", 0.1f))
                            {
                                newPosition = newPosition + new Vector3(grain, 0f, 0f);
                                break;
                            }
                            else if (!CheckObstacle(newPosition + new Vector3(0f, 0f, -grain), "Obstacle", 0.1f))
                            {
                                newPosition = newPosition + new Vector3(0f, 0f, -grain);
                                break;
                            }
                            else if (!CheckObstacle(newPosition + new Vector3(-grain, 0f, 0f), "Obstacle", 0.1f))
                            {
                                newPosition = newPosition + new Vector3(-grain, 0f, 0f);
                                break;
                            }
                            else if (!CheckObstacle(newPosition + new Vector3(0f, 0f, grain), "Obstacle", 0.1f))
                            {
                                newPosition = newPosition + new Vector3(0f, 0f, grain);
                                break;
                            }
                            else
                            {
                                //if none, update with twice the grain to try again
                                grain += originalGrain;
                            }
                        }

                        GameObject newAgent = Instantiate(agent, newPosition, Quaternion.identity) as GameObject;
                        AgentController agentFoundController = newAgent.GetComponent<AgentController>();
                        //change his name
                        int agentName = lineCount - 2;
                        newAgent.name = "agent" + agentName;
                        //change his radius
                        agentFoundController.agentRadius = agentRadius;
                        //change his color
                        newAgent.GetComponent<MeshRenderer>().material.color = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
                        //find the cell in x - z coords, using his name
                        //use the rest of division by cellRadius*2
                        int restDivisionX = (int)(newPosition.x % (cellRadius * 2));
                        int restDivisionZ = (int)(newPosition.z % (cellRadius * 2));

                        //Debug.Log("Agent " + newAgent.name + ": RestX- " + restDivisionX + " -- RestZ- " + restDivisionZ);
                        //Debug.Log("Agent " + newAgent.name + ": newPosition.x- " + newPosition.x + " -- cellRadius- " + cellRadius);

                        int nameX = (int)(newPosition.x - restDivisionX);
                        int nameZ = (int)(newPosition.z - restDivisionZ);

                        GameObject foundCell = GameObject.Find("cell" + nameX + "-" + nameZ);
                        if (foundCell)
                        {
                            agentFoundController.SetCell(foundCell);
                        }
                        else
                        {
                            Debug.Log("Agent " + newAgent.name + ": Cell not found! " + nameX + " - " + nameZ);
                        }
                        //change parent
                        newAgent.transform.parent = parentAgents.transform;

                        //set his goals
                        //first and second information in the file are just the coordinates, which i already have in config file
                        //go 2 in 2, since it is a pair between goal and intention to that goal
                        for (int j = 2; j < entries.Length; j = j + 2)
                        {
                            //there is an empty space on the end of the line, dont know why.
                            if (entries[j] == "") continue;

                            //try to find this goal object
                            GameObject goalFound = GameObject.Find("Goal" + (System.Int32.Parse(entries[j]) + 1));

                            if (goalFound)
                            {
                                agentFoundController.go.Add(goalFound);
                                agentFoundController.intentions.Add(System.Convert.ToSingle(entries[j + 1]));
                                //add a random desire
                                agentFoundController.AddDesire(Random.Range(0f, 1f));
                            }
                        }

                        //add the Looking For state, with a random position
                        GameObject lookingFor = GenerateLookingFor();
                        agentFoundController.go.Add(lookingFor);
                        agentFoundController.intentions.Add(intentionThreshold);
                        agentFoundController.AddDesire(1);

                        //now, we need to reorder our goal/intentions array, so the agent will follow the goal of bigger intention
                        agentFoundController.ReorderGoals();
                    }
                }
                lineCount++;
            }
            while (line != null);
            // Done reading, close the reader and return true to broadcast success    
            theReader.Close();
        }

        // Create a new StreamReader, tell it which file to read and what encoding the file
        //signs file, with signs and their appeals
        theReader = new StreamReader(signsFilename, System.Text.Encoding.Default);

        using (theReader)
        {
            int lineCount = 1;
            // While there's lines left in the text file, do this:
            do
            {
                line = theReader.ReadLine();

                if (line != null)
                {
                    //in first line, it is the qnt agents
                    if (lineCount == 1)
                    {
                        qntSigns = System.Int32.Parse(line);
                    }
                    else
                    {
                        //each line 1 agent, separated by " "
                        string[] entries = line.Split(' ');

                        //sign position
                        Vector3 newPosition = new Vector3(0f, 0f, 0f);
                        //define position based on obstacle vertices
                        if (allObstacles.Length > 0)
                        {
                            if (allObstacles.Length == 1)
                            {
                                //vertices
                                Vector3[] vertices = allObstacles[0].GetComponent<MeshFilter>().mesh.vertices;

                                //check group vertices to find the corners
                                newPosition = vertices[Random.Range(0, vertices.Length - 1)];

                                //while newPosition is inside the already used positions, we try again
                                while (positionsSigns.Contains(newPosition))
                                {
                                    newPosition = vertices[Random.Range(0, vertices.Length - 1)];
                                }
                            }
                            else
                            {
                                //choose an obstacle
                                int obsIndex = Random.Range(0, allObstacles.Length - 1);

                                //vertices
                                Vector3[] vertices = allObstacles[obsIndex].GetComponent<MeshFilter>().mesh.vertices;

                                //check group vertices to find the corners
                                newPosition = vertices[Random.Range(0, vertices.Length - 1)];

                                //while newPosition is inside the already used positions, we try again
                                int flagNewObs = 0;
                                while (positionsSigns.Contains(newPosition))
                                {
                                    //if flag is too high, try another obstacle
                                    if(flagNewObs > vertices.Length * 10)
                                    {
                                        flagNewObs = 0;
                                        obsIndex = Random.Range(0, allObstacles.Length - 1);
                                        vertices = allObstacles[obsIndex].GetComponent<MeshFilter>().mesh.vertices;
                                    }
                                    newPosition = vertices[Random.Range(0, vertices.Length - 1)];
                                    flagNewObs++;
                                }
                            }
                        }

                        //will file bring sign goal
                        GameObject signGoal = GameObject.Find("Goal" + (System.Convert.ToSingle(entries[1]) + 1));

                        DrawSign(newPosition, signGoal, System.Convert.ToSingle(entries[2]));

                        //add this position in the list, so we dont draw here again
                        positionsSigns.Add(newPosition);
                    }
                }
                lineCount++;
            }
            while (line != null);
            // Done reading, close the reader and return true to broadcast success    
            theReader.Close();
            //Need it no more
            positionsSigns.Clear();
        }
    }

    //check if there is Obstacles or something on a given position
    private bool CheckObstacle(Vector3 checkPosition, string tag, float radius)
    {
        Collider[] hitCollider = Physics.OverlapSphere(checkPosition, radius);
        bool returning = false;

        foreach (Collider hit in hitCollider)
        {
            if (hit.gameObject.tag == tag)
            {
                returning = true;
                break;
            }
        }

        return returning;
    }

    //generate new simulation agents and signs
    private void GenerateAnew()
    {
        //start the filesController
        filesController = new FilesController(allSimulations, configFilename, obstaclesFilename, scheduleFilename, exitFilename, signsFilename, goalsFilename, agentsGoalFilename, interactionsFilename);

        //get all cells and goals
        allCells = GameObject.FindGameObjectsWithTag("Cell");
        GameObject[] allGoals = GameObject.FindGameObjectsWithTag("Goal");
        //Debug.Log(allCells.Length);

        for (int i = 0; i < qntGroups; i++)
        {
            float x = 0, z = 0;

            //sort out a cell
            GameObject foundCell = allCells[Random.Range(0, allCells.Length)];

            bool pCollider = true;
            //while collider inside obstacle or player
            while (pCollider)
            {
                //generate random position
                x = Random.Range(foundCell.transform.position.x - cellRadius, foundCell.transform.position.x + cellRadius);
                z = Random.Range(foundCell.transform.position.z - cellRadius, foundCell.transform.position.z + cellRadius);

                //BOTTLENECK EFFECT: ALL NEED TO START BEFORE X = 10
                /*if(x >= 15)
                {
                    pCollider = true;
                    continue;
                }*/

                //see if there are agents in this radius
                pCollider = CheckObstacle(new Vector3(x, 0, z), "Player", 0.5f);
                //if not, still need to check if there is any obstacle
                if (!pCollider)
                {
                    pCollider = CheckObstacle(new Vector3(x, 0, z), "Obstacle", 0.01f);
                }
            }

            //instantiate
            GameObject newAgentGroup = Instantiate(agentGroup, new Vector3(x, 0f, z), Quaternion.identity) as GameObject;
            AgentGroupController newAgentGroupController = newAgentGroup.GetComponent<AgentGroupController>();
            //change its name
            newAgentGroup.name = "agentGroup" + i;
            //set its cell
            newAgentGroupController.cell = foundCell;
            //max agent speed
            newAgentGroupController.maxSpeed = agentMaxSpeed;

            //if using hofstede
            if (useHofstede)
            {
                //calculate the group hofstede
                //CalculateHofstede(int pdi, int mas, int lto, int ing)
                newAgentGroupController.hofstede.CalculateHofstede(50, 0, 20, 50);
                //once we have the hofstede calculated values, we can use the meanSpeed as a mean velocity for the group and calculate the meanSpeedDeviation according the cohesion
                //the more cohesion, the less deviation
                newAgentGroupController.hofstede.SetMeanSpeedDeviation((3 - newAgentGroupController.hofstede.GetMeanCohesion()) / 15);
            }

            //agent group goals
            for (int j = 0; j < allGoals.Length; j++)
            {
                newAgentGroupController.go.Add(allGoals[j]);
                //add a random intention
                newAgentGroupController.intentions.Add(Random.Range(0f, (intentionThreshold - 0.01f)));
                //add a random desire
                newAgentGroupController.desire.Add(Random.Range(0f, 1f));
            }

            //add the Looking For state, with a random position
            GameObject lookingFor = GenerateLookingFor();
            newAgentGroupController.go.Add(lookingFor);
            newAgentGroupController.intentions.Add(intentionThreshold);
            newAgentGroupController.desire.Add(1);

            //reorder following intentions
            newAgentGroupController.ReorderGoals();
        }

        //get the parent Agents
        GameObject agents = GameObject.Find("Agents");
        //get the agent groups
        GameObject[] agentsGroups = GameObject.FindGameObjectsWithTag("AgentGroup");

        //instantiate qntAgents Agents
        for (int i = 0; i < qntAgents; i++)
        {
            //sort out an agent group
            GameObject chosenGroup = agentsGroups[Random.Range(0, agentsGroups.Length)];
            bool alreadyOne = true;
            //since we need to make sure that each group has, at least, 1 agent, we populate the groups 1 by 1 at first
            foreach (GameObject ag in agentsGroups)
            {
                //no agent inside this group
                if (ag.GetComponent<AgentGroupController>().agents.Count == 0)
                {
                    //so, chosen one
                    chosenGroup = ag;
                    alreadyOne = false;
                    break;
                }
            }

            //Debug.Log (x+"--"+z);

            float meanDist = 0.1f;
            //use the mean distance from group Hofstead
            if (useHofstede)
            {
                meanDist = chosenGroup.GetComponent<AgentGroupController>().hofstede.GetMeanDist();
            }

            //generate the agent position
            Vector3 newAgentPosition = GeneratePosition(chosenGroup, !alreadyOne);

            GameObject newAgent = Instantiate(agent, newAgentPosition, Quaternion.identity) as GameObject;
            AgentController newAgentController = newAgent.GetComponent<AgentController>();
            //change his name
            newAgent.name = "agent" + i;
            //random agent color
            newAgentController.SetColor(new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f)));
            //agent cell
            newAgentController.SetCell(chosenGroup.GetComponent<AgentGroupController>().cell);
            //agent radius
            newAgentController.agentRadius = agentRadius;

            newAgent.GetComponent<MeshRenderer>().material.color = newAgentController.GetColor();

            //set agents info which comes from the group
            //group max speed
            newAgentController.maxSpeed = chosenGroup.GetComponent<AgentGroupController>().maxSpeed;

            //if using hofstede, the agent maxSpeed will not be the defined maxSpeed, but the calculated meanSpeed of the group with a variation in the meanSpeedDeviation, defined by the group cohesion
            if (useHofstede)
            {
                float newSpeed = agentMaxSpeed + 1;
                //while speed is invalid
                while (newSpeed > agentMaxSpeed || newSpeed <= 0)
                {
                    newSpeed = Random.Range(chosenGroup.GetComponent<AgentGroupController>().hofstede.GetMeanSpeed() - chosenGroup.GetComponent<AgentGroupController>().hofstede.GetMeanSpeedDeviation(),
                        chosenGroup.GetComponent<AgentGroupController>().hofstede.GetMeanSpeed() + chosenGroup.GetComponent<AgentGroupController>().hofstede.GetMeanSpeedDeviation());
                }
                //set agent speed
                newAgentController.maxSpeed = newSpeed;
            }
            //agent goals from group
            newAgentController.go = chosenGroup.GetComponent<AgentGroupController>().go;
            newAgentController.intentions = chosenGroup.GetComponent<AgentGroupController>().intentions;
            newAgentController.desire = chosenGroup.GetComponent<AgentGroupController>().desire;
            //group goal
            newAgentController.goal = chosenGroup.GetComponent<AgentGroupController>().go[0].transform.position;

            newAgentController.group = chosenGroup;
            chosenGroup.GetComponent<AgentGroupController>().agents.Add(newAgent);

            //change parent
            newAgent.transform.parent = agents.transform;
        }

        //instantiante some signs
        //Vector3 newVertice1 = allObstacles[0].GetComponent<MeshFilter>().mesh.vertices[0];
        //Vector3 newVertice2 = allObstacles[2].GetComponent<MeshFilter>().mesh.vertices[0];

        //it is working, but has problems if the obstacle is rotated (i.e. 180 degrees)
        //Debug.Log(newVertice1+" -- "+ newVertice2);

        //DrawSign(newVertice1, GameObject.Find("Goal1"), Random.Range(0f, 1f));
        //DrawSign(newVertice2, GameObject.Find("Goal2"), Random.Range(0f, 1f));
    }

    private Vector3 GeneratePosition(GameObject group, bool useCenter)
    {
        //generate the position
        Vector3 newPosition = Vector3.zero;
        float grain = group.GetComponent<AgentGroupController>().hofstede.CohesionToHall() / 2;
        //if alreadyOne == false, it is the first agent in this group. So, we can set it on the center
        if (useCenter)
        {
            newPosition = group.transform.position;
        }//else, rand
        else
        {
            newPosition.x = Random.Range(group.transform.position.x - grain, group.transform.position.x + grain);
            newPosition.z = Random.Range(group.transform.position.z - grain, group.transform.position.z + grain);
        }

        //see if there are agents in this radius. if not, instantiante
        bool pCollider = false;
        bool tooFarAway = true;

        for (int j = 0; j < group.GetComponent<AgentGroupController>().agents.Count; j++)
        {
            float distance = Vector3.Distance(newPosition, group.GetComponent<AgentGroupController>().agents[j].transform.position);

            //too near
            if (distance < 0.01f)
            {
                //if (Distance(newPosition, agentsGroups[groupIndex].agents[j].position) < 0.1f) {
                pCollider = true;
                break;
            }//else, we still need to keep agent close to group
            else if (distance < grain * 2)
            {
                tooFarAway = false;
            }
        }

        //if useCenter, need to change the tooFarAway to false, since there is no one to compate with
        if (useCenter)
        {
            tooFarAway = false;
        }

        //even so, if we are inside an obstacle, cannot instantiate either
        //just need to check for obstacle if found no player, otherwise it will not be instantiated anyway
        if (!pCollider)
        {
            pCollider = CheckObstacle(newPosition, "Obstacle", 0.01f);
        }

        //if found a player in the radius, or it is too far away, do not instantiante. try again
        if (pCollider || tooFarAway)
        {
            //try again
            return GeneratePosition(group, useCenter);
        }
        else
        {
            return newPosition;
        }
    }

    //set camera according terrain size
    private void ConfigCamera()
    {
        //camera height and center position
        //if scene is 3:2 scale, we divide for 3 or 2, depending the parameter
        //if they are equal, it is the same scale (i.e. 3:3). So, divide for 2
        float cameraHeight = scenarioSizeX / 3;
        float cameraPositionX = scenarioSizeX / 2;
        float cameraPositionZ = scenarioSizeZ / 2;
        if (scenarioSizeX == scenarioSizeZ)
        {
            cameraHeight = scenarioSizeX / 2;
            cameraPositionX = scenarioSizeX / 2;
            cameraPositionZ = scenarioSizeZ / 2;
        }
        GameObject camera = GameObject.Find("Camera");
        camera.transform.position = new Vector3(cameraPositionX, camera.transform.position.y, cameraPositionZ);
        camera.GetComponent<Camera>().orthographicSize = cameraHeight;
    }

    //generates a new looking for GameObject
    private GameObject GenerateLookingFor()
    {
        GameObject lookingFor = new GameObject();
        lookingFor.name = "LookingFor";
        lookingFor.tag = "LookingFor";
        bool foundPosition = false;
        Vector3 LFPosition = Vector3.zero;
        //while bool. Use this to check for possible obstacles.
        while (!foundPosition)
        {
            foundPosition = true;
            LFPosition = new Vector3(Random.Range(0f, terrain.terrainData.size.x),
                0f, Random.Range(0f, terrain.terrainData.size.z));
            foundPosition = !CheckObstacle(LFPosition, "Obstacle", 0.1f);
        }
        lookingFor.transform.position = LFPosition;
        return lookingFor;
    }

    //checks if simulation is over
    //for that, we check if is there still an agent in the scene
    //if there is none, we clear the scene, update the index, get the new set of files to setup the new simulation and start
    private void EndSimulation()
    {
        GameObject[] allAgents = GameObject.FindGameObjectsWithTag("Player");
        if (allAgents.Length == 0)
        {
            filesController.Finish();

            //set the text
            text.text = "Simulation " + simulationIndex + " Finished!";

            //update simulation index
            simulationIndex++;

            text.text += "\nLoading Simulation " + simulationIndex + "!";

            //if index is >= than allDirs, we have reached the end. So, GAME OVER!!!
            if (simulationIndex >= allDirs.Length)
            {
                gameOver = true;
                Debug.Log("FINISHED!!");

                //set the text
                text.text = "All Simulations Finished!";

                //reset the iterator
                var file = File.CreateText(Application.dataPath + "/" + allSimulations + "/SimulationIterator.txt");
                file.WriteLine("0");
                file.Close();

                //reset the frame count
                file = File.CreateText(Application.dataPath + "/" + allSimulations + "/FrameCount.txt");
                file.WriteLine("0");
                file.Close();

                Application.Quit();
            }
            else
            {
                //else, we keep it going
                var file = File.CreateText(Application.dataPath + "/" + allSimulations + "/SimulationIterator.txt");
                file.WriteLine(simulationIndex.ToString());
                file.Close();

                //update last frame count
                file = File.CreateText(Application.dataPath + "/" + allSimulations + "/FrameCount.txt");
                file.WriteLine(Time.frameCount.ToString());
                file.Close();

                //reset scene
                SceneManager.LoadScene(0);
            }
        }
    }

    //control all chained simulations
    //get the new set of files to setup the new simulation and start
    private void LoadChainSimulation()
    {
        // Create a new StreamReader, tell it which file to read and what encoding the file
        StreamReader theReader = new StreamReader(Application.dataPath + "/" + allSimulations + "/SimulationIterator.txt", System.Text.Encoding.Default);
        using (theReader)
        {
            //we get the updated simulation Index
            string line = theReader.ReadLine();
            simulationIndex = System.Int32.Parse(line);
        }
        theReader.Close();

        theReader = new StreamReader(Application.dataPath + "/" + allSimulations + "/FrameCount.txt", System.Text.Encoding.Default);
        using (theReader)
        {
            //we get the updated simulation Index
            string line = theReader.ReadLine();
            lastFrameCount = System.Int32.Parse(line);
        }
        theReader.Close();
        //Debug.Log(allDirs[simulationIndex]);
        //each directory within the defined config directory has a set of simulation files
        string[] allFiles = Directory.GetFiles(allDirs[simulationIndex]);

        string[] schNam = scheduleFilename.Split('/');
        string[] agfNam = agentsGoalFilename.Split('/');
        string[] extNam = exitFilename.Split('/');
        string[] sigNam = signsFilename.Split('/');
        string[] intNam = interactionsFilename.Split('/');

        for (int i = 0; i < allFiles.Length; i++)
        {
            //just csv and dat files
            if (Path.GetExtension(allFiles[i]) == ".csv" || Path.GetExtension(allFiles[i]) == ".dat")
            {
                if (allFiles[i].Contains(schNam[schNam.Length - 1]))
                {
                    scheduleFilename = allFiles[i];
                }
                else if (allFiles[i].Contains(agfNam[agfNam.Length - 1]))
                {
                    agentsGoalFilename = allFiles[i];
                }
                else if (allFiles[i].Contains(extNam[extNam.Length - 1]))
                {
                    exitFilename = allFiles[i];
                }
                else if (allFiles[i].Contains(sigNam[sigNam.Length - 1]))
                {
                    signsFilename = allFiles[i];
                }
                else if (allFiles[i].Contains(intNam[intNam.Length - 1]))
                {
                    interactionsFilename = allFiles[i];
                }
            }
        }
        //Debug.Log(exitFilename);
        //it is possible that the exit files do not exist yet. So, we check if they were found. If not, we create them
        if (exitFilename.Contains(":") || true)
        {
            //update the filename
            exitFilename = "";
            foreach(string reform in extNam)
            {
                if (reform.Contains(".csv"))
                {
                    exitFilename += reform;
                }
                else
                {
                    exitFilename += reform + "/";
                }
            }
            exitFilename = exitFilename.Replace("0", simulationIndex.ToString());
        }
        //Debug.Log(exitFilename);
        if (agentsGoalFilename.Contains(":") || true)
        {
            //update the filename
            agentsGoalFilename = "";
            foreach (string reform in agfNam)
            {
                if (reform.Contains(".csv"))
                {
                    agentsGoalFilename += reform;
                }
                else
                {
                    agentsGoalFilename += reform + "/";
                }
            }
            agentsGoalFilename = agentsGoalFilename.Replace("0", simulationIndex.ToString());
        }
        if (interactionsFilename.Contains(":") || true)
        {
            //update the filename
            interactionsFilename = "";
            foreach (string reform in intNam)
            {
                if (reform.Contains(".csv"))
                {
                    interactionsFilename += reform;
                }
                else
                {
                    interactionsFilename += reform + "/";
                }
            }
            interactionsFilename = interactionsFilename.Replace("0", simulationIndex.ToString());
        }

        //restart the filesController
        filesController = new FilesController(allSimulations, configFilename, obstaclesFilename, scheduleFilename, exitFilename, signsFilename, goalsFilename, agentsGoalFilename, interactionsFilename);

        LoadConfigFile();
    }

    //draw each obstacle
    private void DrawObstacle(Vector3[] vertices, int[] triangles)
    {
        GameObject obstacles = GameObject.Find("Obstacles");

        GameObject go = new GameObject();
        go.transform.parent = obstacles.transform;

        go.AddComponent<MeshFilter>();
        go.AddComponent<MeshRenderer>();
        MeshFilter mf = go.GetComponent<MeshFilter>();
        var mesh = new Mesh();
        mf.mesh = mesh;

        //set the vertices
        mesh.vertices = vertices;
        mesh.triangles = triangles;

        //obstacle has center at 0x0, so, need to place it obstacleDisplacement forward
        go.transform.position = new Vector3(obstacleDisplacement, 0, obstacleDisplacement);

        go.AddComponent<MeshCollider>();
        //go.GetComponent<MeshCollider>().isTrigger = true;
        go.tag = "Obstacle";
        go.name = "Obstacle";

        //change the static navigation to draw it dinamically
        GameObjectUtility.SetStaticEditorFlags(go, StaticEditorFlags.NavigationStatic);
        GameObjectUtility.SetNavMeshArea(go, 1);
    }

    //draw a sign
    private void DrawSign(Vector3 signPosition, GameObject signGoal, float signAppeal)
    {
        //parent
        GameObject parentSigns = GameObject.Find("Signs");

        GameObject newSign = Instantiate(sign, signPosition, sign.transform.rotation) as GameObject;
        //change name
        newSign.name = "Sign" + GameObject.FindGameObjectsWithTag("Sign").Length;
        //change goal
        newSign.GetComponent<SignController>().SetGoal(signGoal);
        //change appeal
        newSign.GetComponent<SignController>().SetAppeal(signAppeal);
        //change parent
        newSign.transform.parent = parentSigns.transform;
    }

    //draw a goal
    private void DrawGoal(string goalName, Vector3 goalPosition)
    {
        //parent
        GameObject parentGoal = GameObject.Find("Goals");

        GameObject newGoal = Instantiate(goalP, goalPosition, goalP.transform.rotation) as GameObject;
        //change name
        newGoal.name = goalName;
        //change parent
        newGoal.transform.parent = parentGoal.transform;
        //draw a sign on this position too, so if the agent is looking for around, he finds it
        DrawSign(goalPosition, newGoal, 1);
    }

    //dart throwing markers
    private void DartThrowMarkers(int c)
    {
        //use this flag to break the loop if it is taking too long (maybe there is no more space)
        int flag = 0;
        for (int i = 0; i < qntAuxins; i++)
        {
            float x = Random.Range(allCells[c].transform.position.x - cellRadius, allCells[c].transform.position.x + cellRadius);
            float z = Random.Range(allCells[c].transform.position.z - cellRadius, allCells[c].transform.position.z + cellRadius);

            //see if there are auxins in this radius. if not, instantiante
            List<AuxinController> allAuxinsInCell = allCells[c].GetComponent<CellController>().GetAuxins();
            bool canIInstantiante = true;

            for (int j = 0; j < allAuxinsInCell.Count; j++)
            {
                float distanceAA = Vector3.Distance(new Vector3(x, 0f, z), allAuxinsInCell[j].position);

                //if it is too near, i cant instantiante. found one, so can Break
                if (distanceAA < auxinRadius)
                {
                    canIInstantiante = false;
                    break;
                }
            }

            //if i have found no auxin, i still need to check if is there obstacles on the way
            if (canIInstantiante)
            {
                canIInstantiante = !CheckObstacle(new Vector3(x, 0f, z), "Obstacle", auxinRadius / 10);
            }

            //canIInstantiante???                
            if (canIInstantiante)
            {
                AuxinController newAuxin = new AuxinController();
                //change his name
                newAuxin.name = "auxin" + c + "-" + i;
                //this auxin is from this cell
                newAuxin.SetCell(allCells[c]);
                //set position
                newAuxin.position = new Vector3(x, 0f, z);

                //add this auxin to this cell
                allCells[c].GetComponent<CellController>().AddAuxin(newAuxin);

                //reset the flag
                flag = 0;
            }
            else
            {
                //else, try again
                flag++;
                i--;
            }

            //if flag is above qntAuxins (*2 to have some more), break;
            if (flag > qntAuxins * 2)
            {
                //reset the flag
                flag = 0;
                break;
            }
        }
    }
}