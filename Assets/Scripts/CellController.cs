﻿using UnityEngine;
using System.Collections.Generic;

public class CellController : MonoBehaviour {

    public List<AuxinController> myAuxins;

    public void StartList()
    {
        myAuxins = new List<AuxinController>();
    }
    
    //add a new auxin on myAuxins
    public void AddAuxin(AuxinController auxin)
    {
        myAuxins.Add(auxin);
    }

    //return all auxins in this cell
    public List<AuxinController> GetAuxins() {
        return myAuxins;     
    }
}
