﻿public class HofstedeClass {
    //constructors
    public HofstedeClass()
    {
	    meanAngVar = meanCohesion = meanDist = meanSpeed = 0;
	    numGroups = 1;
    }

    public HofstedeClass(int newNumGroups)
    {
	    meanAngVar = meanCohesion = meanDist = meanSpeed = 0;
	    numGroups = newNumGroups;
    }

    //number of groups
    public int numGroups;
    //Hofstede values
    private float meanDist, meanCohesion, meanAngVar, meanSpeed, meanSpeedDeviation;

    //calculate the hofstede values
    public void CalculateHofstede(int pdi, int mas, int lto, int ing)
    {
        meanDist = ((100 - pdi) * (1.2f) / 100) * numGroups;
        meanCohesion = (((100 - mas) * 3) / 100) * numGroups;
        meanAngVar = ((100 - lto) / 100) * numGroups;
        meanSpeed = (ing * 1.4f / 100) * numGroups;
    }

    //transform the cohesion value in a Hall distance value
    //Higher cohesion: 0,45m
    //Lower cohesion: 1,20m
    public float CohesionToHall()
    {
        return (float)(1.2f - (meanCohesion * 0.25f));
    }

    //Getters and Setters
    public float GetMeanDist()
    {
        return meanDist;
    }
    public void SetMeanDist(float value)
    {
        meanDist = value;
    }
    public float GetMeanCohesion()
    {
        return meanCohesion;
    }
    public void SetMeanCohesion(float value)
    {
        meanCohesion = value;
    }
    public float GetMeanAngVar()
    {
        return meanAngVar;
    }
    public void SetMeanAngVar(float value)
    {
        meanAngVar = value;
    }
    public float GetMeanSpeed()
    {
        return meanSpeed;
    }
    public void SetMeanSpeed(float value)
    {
        meanSpeed = value;
    }
    public float GetMeanSpeedDeviation()
    {
        return meanSpeedDeviation;
    }
    public void SetMeanSpeedDeviation(float value)
    {
        meanSpeedDeviation = value;
    }
}
