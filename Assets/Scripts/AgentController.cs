﻿using UnityEngine;
using System.Collections.Generic;

public class AgentController : MonoBehaviour {
	//agent radius
	public float agentRadius;
	//agent speed
	public Vector3 speed;
    //max speed
    public float maxSpeed;
    //goals schedule
    public List<GameObject> go;
    //goals intentions
    public List<float> intentions;
    //goals desire
    public List<float> desire;
    //auxins distance vector from agent
    public List<Vector3> vetorDistRelacaoMarcacao;
    //field of view, to see signs
    public float fieldOfView;
    //to avoid locks
    public bool changePosition = true;
    //agent cell
    public GameObject cell;
    //goal position
    public Vector3 goal;
    //agent group
    public GameObject group;

    //list with all auxins in his personal space
    private List<AuxinController> myAuxins;
    //agent color
    private Color color;
    //path
    private UnityEngine.AI.NavMeshPath path;
    //time elapsed (to calculate path just between an interval of time)
    private float elapsed;
    //calc path?
    private bool calcPath = true;
    //all signs
    private GameObject[] allSigns;
    // to calculate var m (blocks recalculation)
    private bool denominadorW  = false;
    // to calculate var m (blocks recalculation)
    private float valorDenominadorW;
    //orientation vector (movement)
    private Vector3 m;
    //diff between goal and agent
    private Vector3 diff;
    //diff module
    private float diffMod;
    //goal vector (diff / diffMod)
    private Vector3 g; 

    //on destroy agent, free auxins
    void OnDestroy()
    {
        //iterate all cell auxins to check distance between auxins and agent
        for (int i = 0; i < myAuxins.Count; i++)
        {
            myAuxins[i].ResetAuxin();
        }

        //clear it
        myAuxins.Clear();
    }

    void Awake(){
        //set inicial values
		myAuxins = new List<AuxinController>();
        desire = new List<float>();       
        valorDenominadorW = 0;
        denominadorW = false;
        elapsed = 0f;
    }

    void Start() {
        goal = go[0].transform.position;
        diff = goal - transform.position;
        diffMod = Vector3.Distance(diff, new Vector3(0f, 0f, 0f));
        g = diff / diffMod;
        path = new UnityEngine.AI.NavMeshPath();

        //get all signs, on the environment
        allSigns = GameObject.FindGameObjectsWithTag("Sign");
    }

    void Update() {
        //clear agent´s informations
        ClearAgent();
        // Update the way to the goal every second.
        elapsed += Time.deltaTime;
        
        //right now: updating every iteration, since calcPath is always true
        if (elapsed > 1f || calcPath)
        {
            elapsed -= 1f;
            //calculate agent path
            UnityEngine.AI.NavMesh.CalculatePath(transform.position, go[0].transform.position, UnityEngine.AI.NavMesh.AllAreas, path);
            
            //update his goal
            goal = new Vector3(path.corners[1].x, 0f, path.corners[1].z);
            diff = goal - transform.position;
            diffMod = Vector3.Distance(diff, new Vector3(0f, 0f, 0f));
            g = diff / diffMod;

            //dont calculate again
            //calcPath = false;
        }

        //Debug.Log(path.corners.Length);
        //just to draw the path
        for (int i = 0; i < path.corners.Length - 1; i++)
        {
            Debug.DrawLine(path.corners[i], path.corners[i + 1], Color.red);
        }

        //check interaction with possible signs
        CheckSignsInView();
    }

    /*void OnGUI() {
        var pos = Camera.current.WorldToScreenPoint(transform.position);
        var rect = new Rect(pos.x-20, Screen.height - pos.y-20, 40, 40);
        GUI.Label(rect, name);
    }*/

    //clear agent´s informations
    public void ClearAgent()
    {
        //re-set inicial values
        valorDenominadorW = 0;
        vetorDistRelacaoMarcacao.Clear();
        denominadorW = false;
        m = new Vector3(0f, 0f, 0f);
        diff = goal - transform.position;
        diffMod = Vector3.Distance(diff, new Vector3(0, 0, 0));
        g = diff / diffMod;

        changePosition = true;
    }

    //walk
    public void Walk()
    {
        transform.Translate(speed * Time.deltaTime, Space.World);
        //Debug.Log(Time.deltaTime);
        //transform.Translate(speed, Space.World);
        //Debug.Log(speed);
    }

    //The calculation formula starts here
    //the ideia is to find m=SUM[k=1 to n](Wk*Dk)
    //where k iterates between 1 and n (number of auxins), Dk is the vector to the k auxin and Wk is the weight of k auxin
    //the weight (Wk) is based on the degree resulting between the goal vector and the auxin vector (Dk), and the
    //distance of the auxin from the agent
    public void CalculateMotionVector()
    {
        //for each agent´s auxin
        for (int k = 0; k < vetorDistRelacaoMarcacao.Count; k++)
        {
            //calculate W
            float valorW = CalculateWeight(k);
            if (valorDenominadorW < 0.0001)
            //if (valorDenominadorW == 0)
                valorW = 0.0f;
            
            //sum the resulting vector * weight (Wk*Dk)
            m += valorW * vetorDistRelacaoMarcacao[k]*maxSpeed;            
        }
    }

    //calculate speed vector    
    public void CalculateSpeed()
    {
        Debug.DrawLine(transform.position, transform.position + m, Color.green);
        //distance between movement vector and origin
        float moduloM = Vector3.Distance(m, new Vector3(0, 0, 0));

        //multiply for PI
        //float s = moduloM * 3.14f;
        float s = moduloM;
        //Debug.Log(s);
        float thisMaxSpeed = maxSpeed;

        float time = Time.deltaTime;

        //actual distance from group center position
        /*float actualDistance = Vector3.Distance(transform.position, group.transform.position);
        //movement prediction distance
        Vector3 prediction = new Vector3(transform.position.x + ((s * (m.x / moduloM)) * time), transform.position.y + ((s * (m.y / moduloM)) * time), transform.position.z + ((s * (m.z / moduloM)) * time));
        float movementPredctionDistance = Vector3.Distance(prediction, group.transform.position);
        //test the movement prediction. If the agent will be too far from group center position, reduce his maxSpeed
        if (movementPredctionDistance > 5 && movementPredctionDistance > actualDistance)
        {
            thisMaxSpeed /= 5;
        }*/

        //if it is bigger than maxSpeed, use maxSpeed instead
        if (s > thisMaxSpeed)
            s = thisMaxSpeed;
        //Debug.Log("vetor M: " + m + " -- modulo M: " + s);
        if (moduloM > 0.0001)
        {
            //calculate speed vector
            speed = s * (m / moduloM);
        }
        else
        {
            //else, he is idle
            speed = new Vector3(0, 0, 0);
        }
    }

    //find all auxins near him (Voronoi Diagram)
    //call this method from game controller, to make it sequential for each agent
    public void FindNearAuxins(float cellRadius){
		//clear all agents auxins, to start again for this iteration
		myAuxins.Clear ();

        //check all auxins on agent's cell
        CheckAuxinsCell(cell);

        //find all neighbours cells
        int startX = (int)(cell.transform.position.x - (cellRadius * 2f));
        int startZ = (int)(cell.transform.position.z - (cellRadius * 2f));
        int endX = (int)(cell.transform.position.x + (cellRadius * 2f));
        int endZ = (int)(cell.transform.position.z + (cellRadius * 2f));
        //distance from agent to cell, to define agent new cell
        float distanceToCell = Vector3.Distance(transform.position, cell.transform.position);
        //Debug.Log(gameObject.name+" -- StartX: "+startX+" -- StartZ: "+startZ+" -- EndX: "+endX+" -- EndZ: "+endZ);
        //iterate to find the cells
        //2 in 2, since the radius of each cell is 1 = diameter 2
        for(float i = startX; i <= endX; i = i + (cellRadius * 2))
        {
            for (float j = startZ; j <= endZ; j = j + (cellRadius * 2))
            {
                float nameX = i - cellRadius;
                float nameZ = j - cellRadius;
                //find the cell
                GameObject neighbourCell = GameObject.Find("cell"+nameX+"-"+nameZ);

                //if it exists..
                if (neighbourCell)
                {
                    //check all auxins on this cell
                    CheckAuxinsCell(neighbourCell);

                    //see distance to this cell
                    //if it is lower, the agent is in another(neighbour) cell
                    float distanceToNeighbourCell = Vector3.Distance(transform.position, neighbourCell.transform.position);
                    if (distanceToNeighbourCell < distanceToCell)
                    {
                        distanceToCell = distanceToNeighbourCell;
                        SetCell(neighbourCell);
                    }
                }
            }
        }
    }

    //reorder goals/intentions
    public void ReorderGoals() {
        for (int i = 0; i < intentions.Count; i++)
        {
            for (int j = i+1; j < intentions.Count; j++)
            {
                //if j element is bigger, change
                if (intentions[i] < intentions[j])
                {
                    //reorder intentions
                    float temp = intentions[j];
                    intentions[j] = intentions[i];
                    intentions[i] = temp;

                    //reorder desires
                    float tempD = desire[j];
                    desire[j] = desire[i];
                    desire[i] = tempD;

                    //reorder goals
                    GameObject tempG = go[j];
                    go[j] = go[i];
                    go[i] = tempG;
                }
            }
        }
    }

    //check if the goals are the same as other agent
    //update: just check the actual goal
    public bool CheckGoals(GameObject otherAgent)
    {
        
        //for default, get the go[0]. If the first is lookingFor, ignore it and get next (go[1])
        string goalName = go[0].name;
        if (go[0].tag == "LookingFor" && go.Count > 1)
        {
            goalName = go[1].name;
        }

        //now, find out if otherAgent has this one as next goal too
        //follow the same logic as before
        string otherGoalName = otherAgent.GetComponent<AgentController>().go[0].name;
        if (otherAgent.GetComponent<AgentController>().go[0].tag == "LookingFor" && otherAgent.GetComponent<AgentController>().go.Count > 1)
        {
            otherGoalName = otherAgent.GetComponent<AgentController>().go[1].name;
        }

        //now, just compare
        if (goalName == otherGoalName)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //check if there is a sign in the agent Field of View
    private void CheckSignsInView()
    {
        //get all signs on scene
        //for each one of them, check the distance between it and the agent
        bool reorder = false;
        foreach (GameObject sign in allSigns)
        {
            float distance = Vector3.Distance(transform.position, sign.transform.position);
            //if distance <= agent field of view, the sign may affect the agent
            if (distance <= fieldOfView)
            {
                //now, lets see if this sign is from a goal that our agent has intention to go
                for (int i = 0; i < go.Count; i++)
                {
                    if (go[i] == sign.GetComponent<SignController>().GetGoal())
                    {
                        //well, lets do the interaction
                        Interaction(sign, distance, i);
                        reorder = true;
                        break;
                    }
                }
            }
        }

        //reorder our goals
        if (reorder)
        {
            ReorderGoals();
        }
    }

    //make the interaction between the agent and the signs he can see
    private void Interaction(GameObject sign, float distance, int index)
    {
        float deltaIntention;

        //alfa -> interaction environment
        float alfa;
        alfa = 1.0f / distance;
        if (alfa > 1)
            alfa = 1;

        // From the model in Bosse et al 2014 limited for 2 agents
        //float Sq = (intentions[index]);
        //sign intention will be always 1
        float Sq = 1;

        float gama = desire[index] * alfa * sign.GetComponent<SignController>().GetAppeal();

        deltaIntention = gama * (Sq - intentions[index]);

        intentions[index] = intentions[index] + deltaIntention;

        //get the game controller to write the file
        GameController gameController = GameObject.Find("GameController").GetComponent<GameController>();
        gameController.filesController.SaveInteractionsFile(gameObject, sign, gameController.lastFrameCount, deltaIntention, intentions[index]);
        //Debug.Log(gameObject.name+"--"+deltaIntention);
    }

    //calculate W
    private float CalculateWeight(int indiceRelacao)
    {
        //calculate F (F is part of weight formula)
        float valorF = CalculateF(indiceRelacao);

        if (!denominadorW)
        {
            valorDenominadorW = 0f;

            //for each agent´s auxin
            for (int k = 0; k < vetorDistRelacaoMarcacao.Count; k++)
            {
                //calculate F for this k index, and sum up
                valorDenominadorW += CalculateF(k);
            }
            denominadorW = true;
        }

        float retorno = valorF / valorDenominadorW;
        return retorno;
    }

    //calculate F (F is part of weight formula)
    private float CalculateF(int indiceRelacao)
    {
        //distance between auxin´s distance and origin (dont know why origin...)
        float moduloY = Vector3.Distance(vetorDistRelacaoMarcacao[indiceRelacao], new Vector3(0, 0, 0));
        //distance between goal vector and origin (dont know why origin...)
        float moduloX = Vector3.Distance(g, new Vector3(0, 0, 0));
        //vector * vector
        float produtoEscalar = vetorDistRelacaoMarcacao[indiceRelacao].x * g.x + vetorDistRelacaoMarcacao[indiceRelacao].y * g.y + vetorDistRelacaoMarcacao[indiceRelacao].z * g.z;

        if (moduloY < 0.00001)
        {
            return 0.0f;
        }

        //return the formula, defined in tesis/paper
        float retorno = (float)((1.0 / (1.0 + moduloY)) * (1.0 + ((produtoEscalar) / (moduloX * moduloY))));
        return retorno;
    }

    //check auxins on a cell
    private void CheckAuxinsCell(GameObject checkCell)
    {
        //get all auxins on my cell
        List<AuxinController> cellAuxins = checkCell.GetComponent<CellController>().GetAuxins();

        //iterate all cell auxins to check distance between auxins and agent
        for (int i = 0; i < cellAuxins.Count; i++)
        {
            //see if the distance between this agent and this auxin is smaller than the actual value, and inside agent radius
            float distance = Vector3.Distance(transform.position, cellAuxins[i].position);
            if (distance < cellAuxins[i].GetMinDistance() && distance <= agentRadius)
            {
                //take the auxin!!
                //if this auxin already was taken, need to remove it from the agent who had it
                if (cellAuxins[i].taken == true)
                {
                    GameObject otherAgent = cellAuxins[i].GetAgent();
                    otherAgent.GetComponent<AgentController>().myAuxins.Remove(cellAuxins[i]);
                }
                //auxin is taken
                cellAuxins[i].taken = true;
                //auxin has agent
                cellAuxins[i].SetAgent(this.gameObject);
                //update min distance
                cellAuxins[i].SetMinDistance(distance);
                //update my auxins
                myAuxins.Add(cellAuxins[i]);
            }
        }
    }

    //GET-SET
    public Color GetColor(){
		return this.color;
	}
	public void SetColor(Color color){
		this.color = color;
	}
    public GameObject GetCell()
    {
        return this.cell;
    }
    public void SetCell(GameObject cell)
    {
        this.cell = cell;
    }
    //add a new auxin on myAuxins
    public void AddAuxin(AuxinController auxin)
    {
        myAuxins.Add(auxin);
    }
    //return all auxins in this cell
    public List<AuxinController> GetAuxins()
    {
        return myAuxins;
    }
    //add a new desire on desire
    public void AddDesire(float newDesire)
    {
        desire.Add(newDesire);
    }
    //remove a desire on desire
    public void RemoveDesire(int index)
    {
        desire.RemoveAt(index);
    }
}
