﻿//used to do things in Editor time, like prepare the scenario
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof (GameController))]
public class EditorController : Editor {
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var rect = GUILayoutUtility.GetRect(500, 40);

        //pre process for set all cells and obstacles
        if (GUI.Button(rect, "Pre-Process"))
        {
            PreProccess();
        }

        //check the group vertices to find the corners
        /*rect = GUILayoutUtility.GetRect(500, 40);
        if (GUI.Button(rect, "Check Group Vertices"))
        {
            CheckGroupVertices();
        }*/

        //generate the metric for Purdue
        /*rect = GUILayoutUtility.GetRect(500, 40);
        if (GUI.Button(rect, "Generate Metric"))
        {
            GenerateMetric();
        }*/

        //save OBJ File
        /*rect = GUILayoutUtility.GetRect(500, 40);
        if (GUI.Button(rect, "Save new OBJ File"))
        {
            SaveOBJFile();
        }*/
    }

    public void PreProccess() {
        if ((target as GameController).loadConfigFile)
        {
            (target as GameController).LoadCellsAuxins();
        }
        else
        {
            //(target as GameController).DrawObstacles();
            //(target as GameController).DrawCells();
            //(target as GameController).PlaceAuxins();
        }
    }

    /*public void CheckGroupVertices()
    {
        (target as GameController).CheckGroupVertices();
    }

    public void GenerateMetric() {
        (target as GameController).GenerateMetric();
    }

    public void SaveOBJFile()
    {
        (target as GameController).SaveOBJFile();
    }*/
}
