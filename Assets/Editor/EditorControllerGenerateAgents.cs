﻿//used to do things in Editor time, like prepare the scenario
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof (GenerateAgents))]
public class EditorControllerGenerateAgents : Editor {
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        var rect = GUILayoutUtility.GetRect(500, 40);

        //pre process for set all cells and obstacles
        if (GUI.Button(rect, "Generate Agents"))
        {
            CreateAgents();
        }

        rect = GUILayoutUtility.GetRect(500, 40);
        //pre process for set all cells and obstacles
        if (GUI.Button(rect, "Read File"))
        {
            ReadFile();
        }
    }

    public void CreateAgents() {
        (target as GenerateAgents).CreateAgents();
    }

    public void ReadFile()
    {
        (target as GenerateAgents).ReadFile();
    }
}
