﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class WalkController : MonoBehaviour {
    //walking filename
    //public string walkFilename;
    //how many FPS will we be running? (default = 0 -> no limit)
    public int fps;

    //walking file
    //private StreamReader theReader;
    //all frame positions
    public List<Vector3> allPositionsByFrame;
    //boy?
    public bool isBoy = true;
    //to make the timer
    private float elapsed = 0;
    //last position (to estimate the speed)
    private Vector3 lastSpeed = Vector3.zero;

    /*void OnDestroy()
    {
        theReader.Close();
    }*/

    void Awake()
    {
        //StartList();
    }

    public void StartList() {
        allPositionsByFrame = new List<Vector3>();
    }

    // Use this for initialization
    void Start () {
        //place the agent at the start position
        transform.position = allPositionsByFrame[0];
        allPositionsByFrame.RemoveAt(0);
    }
	
	// Update is called once per frame
	void Update () {
        float interval = 1 / (float)fps;

        if (allPositionsByFrame.Count > 0)
        {
            //if it is free to go, just go
            if(fps == 0)
            {
                UpdatePosition();
            }
            else{
                //update timer
                elapsed += Time.deltaTime;
                //if the difference is enough, update position
                if (elapsed >= interval)
                {
                    UpdatePosition();

                    //reset timer
                    elapsed = 0;
                }
            }
        }else
        {
            if (isBoy)
            {
                gameObject.GetComponent<Animation>().Play("IdleNewBoy");
            }
            else
            {
                gameObject.GetComponent<Animation>().Play("IdleNewGirl");
            }
        }
    }

    private void UpdatePosition()
    {
        //rotate to the next position direction
        //transform.LookAt(allPositionsByFrame[0]);
        transform.LookAt(new Vector3(allPositionsByFrame[0].x, transform.position.y, allPositionsByFrame[0].z));
        transform.Rotate(transform.up * 180);
        //update agent position by each frame, getting it from the allPositionsByFrame, which was read from file
        transform.position = allPositionsByFrame[0];

        //stair code
        //if it is within the stair boundary area, need to diminish its Y axis
        if (transform.position.x > 99.465f && transform.position.x < 109.277f && transform.position.z > 93.653f && transform.position.z < 100.2f)
        {
            if (transform.position.z > 99.626f && transform.position.z < 100.2f)
            {
                transform.position = new Vector3(transform.position.x, -0.35f, transform.position.z);
            }
            else if (transform.position.z > 99.166f && transform.position.z < 99.626f)
            {
                transform.position = new Vector3(transform.position.x, -0.32f, transform.position.z);
            }
            else if (transform.position.z > 98.703f && transform.position.z <= 99.166f)
            {
                transform.position = new Vector3(transform.position.x, -0.29f, transform.position.z);
            }
            else if (transform.position.z > 98.246f && transform.position.z <= 98.703f)
            {
                transform.position = new Vector3(transform.position.x, -0.27f, transform.position.z);
            }
            else if (transform.position.z > 97.787f && transform.position.z <= 98.246f)
            {
                transform.position = new Vector3(transform.position.x, -0.25f, transform.position.z);
            }
            else if (transform.position.z > 97.315f && transform.position.z <= 97.787f)
            {
                transform.position = new Vector3(transform.position.x, -0.23f, transform.position.z);
            }
            else if (transform.position.z > 96.866f && transform.position.z <= 97.315f)
            {
                transform.position = new Vector3(transform.position.x, -0.21f, transform.position.z);
            }
            else if (transform.position.z > 96.404f && transform.position.z <= 96.866f)
            {
                transform.position = new Vector3(transform.position.x, -0.18f, transform.position.z);
            }
            else if (transform.position.z > 95.717f && transform.position.z <= 96.404f)
            {
                transform.position = new Vector3(transform.position.x, -0.15f, transform.position.z);
            }
            else if (transform.position.z > 95.029f && transform.position.z <= 95.717f)
            {
                transform.position = new Vector3(transform.position.x, -0.13f, transform.position.z);
            }
            else if (transform.position.z > 94.3415f && transform.position.z <= 95.029f)
            {
                transform.position = new Vector3(transform.position.x, -0.09f, transform.position.z);
            }
            else if (transform.position.z > 93.653f && transform.position.z <= 94.3415f)
            {
                transform.position = new Vector3(transform.position.x, -0.05f, transform.position.z);
            }
        }
        else
        {
            transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        }

        //estimate velocity
        float estimateVelocity = Vector3.Distance(transform.position, lastSpeed);

        //if too slow and walking, play stop
        if(estimateVelocity < 0.01f)
        {
            //System.Collections.ArrayList states = new System.Collections.ArrayList(gameObject.GetComponent<Animation>().col);
            //List<AnimationState> ass = new List<AnimationState>(gameObject.GetComponent<Animation>() as );
            //gameObject.GetComponent<Animation>().;
            if (isBoy)
            {
                gameObject.GetComponent<Animation>().Play("IdleNewBoy");
            }else
            {
                gameObject.GetComponent<Animation>().Play("IdleNewGirl");
            }
        }//else, walking
        else// if (gameObject.GetComponent<Animation>().IsPlaying("IdleNewBoy") || gameObject.GetComponent<Animation>().IsPlaying("IdleNewGirl"))
        {
            if (isBoy)
            {
                gameObject.GetComponent<Animation>().Play("WalkNewBoy");
            }
            else
            {
                gameObject.GetComponent<Animation>().Play("WalkNewGirl");
            }
        }

        //update lastSpeed
        lastSpeed = allPositionsByFrame[0];

        //remove this index
        allPositionsByFrame.RemoveAt(0);
    }
}
